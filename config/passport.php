<?php

return [

    'client' => [
        'id' => env('OME_PASSPORT_CLIENT_ID'),
        'secret' => env('OME_PASSPORT_CLIENT_SECRET'),
    ],

];
