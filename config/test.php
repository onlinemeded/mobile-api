<?php

return [

    'username' => env('TEST_USERNAME', 'will@williamyoumans.com'),
    'password' => env('TEST_PASSWORD', 'flashback'),
    'id'       => env('TEST_ID', 1),
    'uid'      => env('TEST_SOCIAL_UID', null),
    'provider' => env('TEST_SOCIAL_PROVIDER', 'facebook'),
];
