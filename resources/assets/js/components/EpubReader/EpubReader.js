import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Swipeable from 'react-swipeable'
import Sidebar from "react-sidebar";
import { EpubView } from 'react-reader';
import { withBaseIcon } from 'react-icons-kit'
import { ic_format_list_bulleted } from 'react-icons-kit/md/ic_format_list_bulleted'
import { ic_error } from 'react-icons-kit/md/ic_error';
import { spinner } from 'react-icons-kit/fa/spinner'


import {
  epubReaderStyles as styles,
  sidebarStyles,
} from './EpubReaderStyles';

const Icon16 = withBaseIcon({ size: 16, style: { color: '#29434e' } })
const Icon24 = withBaseIcon({ size: 24, style: { color: '#29434e' } })
const Icon48 = withBaseIcon({ size: 48, style: { color: '#29434e' } })

class EpubReader extends Component {
  constructor(props) {
    super(props)
    this.rendition = null;
    this.readerRef = React.createRef();
    this.state = {
      navOpen: false,
    }
  }
  
  getRendition(rendition) {
    this.rendition = rendition;
  }

  renderLoadingView() {
    return (
      <div style={styles.loadingContainer}>
        <div style={styles.loadingIcon}>
          <Icon24 icon={spinner} />
        </div>
        <span style={styles.loadingText}>Loading</span>
      </div>
    )
  }

  renderError() {
    return (
        <div style={styles.errorContainer}>
            <Icon48 icon={ic_error} />
            <span style={styles.errorText}>Something went wrong :(</span>
        </div>
    )
  }

  nextPage() {
    const node = this.readerRef.current
    node.nextPage();
  }

  prevPage() {
    const node = this.readerRef.current
    node.prevPage()
  }

  handleSwipeAreaTap() {
    const { navOpen } = this.state;
    if (navOpen) this.setState({ navOpen: false });
  }

  toggleNav() {
    const { navOpen } = this.state;
    this.setState({ navOpen: !navOpen })
  }

  onTocChange(toc) {
    this.setState({ toc })
  }

  handleLocationChange(locIdx) {
    this.rendition.display(locIdx)
    this.toggleNav();
  }
  
  renderSideBarContent() {    
    const { toc } = this.state;
    return (
      <div style={styles.navContainer}>
        <div style={styles.navHeader}>
          <span style={styles.navHeaderText}>Table of Contents</span>
        </div> 
        <div style={styles.navBody}>
          {
            _.map(toc, (item, i) => {
              return (
                <button
                  key={i}
                  style={styles.bodyItem}
                  onClick={() => this.handleLocationChange(i)}
                >
                  <span style={styles.bodyText}>{item.label}</span>
                </button>
              )
            })
          }
        </div>
      </div>
    ); 
  }

  render() {
    const {
      url,
      title,
    } = this.props;

    const { navOpen } = this.state;
    if (!url || !title) return this.renderError();

    return (
      <Sidebar
        sidebar={this.renderSideBarContent()}
        docked={navOpen}
        transitions
        shadow
        styles={sidebarStyles}
      >
        <div style={styles.container}>
          <button
            style={styles.navButton}
            onClick={() => this.toggleNav()}
          >
            <Icon16 icon={ic_format_list_bulleted} />
          </button>
          <Swipeable
            onSwipedRight={() => this.prevPage()}
            onSwipedLeft={() => this.nextPage()}
            onTap={() => this.handleSwipeAreaTap()}
            trackMouse
          >
            <div style={styles.epubContainer}>
                <EpubView
                  ref={this.readerRef}
                  url={url}
                  loadingView={this.renderLoadingView()}
                  getRendition={r => this.getRendition(r)}
                  tocChanged={toc => this.onTocChange(toc)}
                />
            </div>
            <div style={styles.swipeArea} />
          </Swipeable>
        </div>
      </Sidebar>
    );
  }
}


if (document.getElementById('epubReader')) {
    const element = document.getElementById('epubReader');
    const props =  Object.assign({}, element.dataset);

    ReactDOM.render(<EpubReader {...props} />, element);
}
