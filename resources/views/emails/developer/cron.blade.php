@extends('layouts.email')
@section('content')

<p>A cron job has been executed for OnlineMedEd.org.</p>

<p>
    <strong>Name:</strong> {{ $name }}
</p>

<p>
    <strong>Execution Time:</strong> {{ $duration }} seconds
</p>

<p>
    {{ $details }}
</p>

@stop

@section('signature')
<p>Regards,<br>
{{ $environment }} server</p>
@stop
