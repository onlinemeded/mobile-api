@extends('layouts.email')
@section('content')
    <h3>User Info: </h3>
    <p>user id: {{$user->id}}</p>
    <p>user email: {{$user->email}}</p>
    <br>
    <h3>Feedback: </h3>
    <p style="text-align: center;">"{{$feedbackText}}"</p>
    <br>
@stop

@section('signature')
    <p>The ome-mobile team @ Onlinemeded</p>
@stop