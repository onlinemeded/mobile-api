#!/usr/bin/env bash

DATE=`date '+%Y_%m_%d_%H_%M_%S'`
REPORT=jmeter_report_mobile_api_$DATE
REPORT_PATH=$TMPDIR/$REPORT

$JMETER_HOME/bin/jmeter -n -t apiLoadTest.jmx -l $TMPDIR/jmeter_$DATE.log -e -o $REPORT_PATH

aws s3 cp $REPORT_PATH s3://onlinemeded-developer/reports/$REPORT --recursive --acl public-read

open https://s3-us-west-2.amazonaws.com/onlinemeded-developer/reports/$REPORT/index.html
