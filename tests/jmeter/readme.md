1. install JMeter
`curl apache-jmeter-5.1.1.zip -o`

2. set JMETER_HOME in your profile
`vi ~/.bash_profile`

3. install aws cli if you want to publish reports to s3
[awscli](https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html)
```
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

aws configure
```

4. run `JMETER_HOME/bin/jmeter` and open `apiLoadTest.jmx in JMeter and set the desired LOOPS and THREADS in "User Defined Variables"

5. run load test and upload results to s3
`./load_test.sh`
