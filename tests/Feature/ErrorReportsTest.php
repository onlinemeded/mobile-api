<?php

namespace Tests\Feature;

use App\ErrorReport;

class ErrorReportsTest extends TestCase
{

    protected $testReport = [
        'id'     => 1,
        'type'   => 'flashcard',
        'reason' => 'Test reason',
        'sub_reason' => 'Test sub_reason',
    ];

    public function testNotAuthenticated()
    {

        $response = $this->client->post('error-reports');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testInvalidReasonInput()
    {
        $token = $this->getToken();

        $testReport = $this->testReport;
        unset($testReport['reason']);

        $response = $this->client->post('error-reports?token=' . urlencode($token), [
            'form_params' => $testReport,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->reason));
        $this->assertTrue(count($json->errors->reason) > 0);
    }

    public function testValidInput()
    {
        $token    = $this->getToken();
        $response = $this->client->post('error-reports?token=' . urlencode($token), [
            'form_params' => $this->testReport,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $errorReport = ErrorReport::where('reason', $this->testReport['reason'])->first();
        // clear test report
        ErrorReport::where('reason', $this->testReport['reason'])->delete();

        $this->assertTrue(is_object($errorReport));
        $this->assertEquals($errorReport->object_id, $this->testReport['id']);
        $this->assertEquals($errorReport->user_id, config('test.id'));
        $this->assertEquals($errorReport->reason, $this->testReport['reason']);
    }
}
