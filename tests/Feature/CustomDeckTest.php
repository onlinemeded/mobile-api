<?php

namespace Tests\Feature;

use App\CustomDeck;
use App\Flashcard;

class CustomDeckTest extends TestCase
{
    protected $testDeck = [
        'name'          => 'Test Custom Deck',
        'flashcard_ids' => [1, 3, 5, 7, 9, 10, 8, 6, 4, 2],
        'updated_at'    => '2016-12-16 06:27:12',
        'created_at'    => '2016-12-01 10:12:30',
    ];

    private function createTestDeck()
    {
        $this->deleteTestDeck();

        $deck = CustomDeck::create([
            'name'    => $this->testDeck['name'],
            'user_id' => config('test.id'),
        ]);

        // remove any lingering associations
        $deck->flashcards()->sync([]);

        foreach ($this->testDeck['flashcard_ids'] as $flashcard_id) {
            if ($flashcard = Flashcard::find($flashcard_id)) {
                $deck->flashcards()->attach($flashcard->id);
            }
        }

        return $deck;
    }

    private function deleteTestDeck()
    {
        $decks = CustomDeck::where('user_id', config('test.id'))->get();

        foreach ($decks as $deck) {
            // remove any lingering associations
            $deck->flashcards()->sync([]);
            $deck->delete();
        }

        return true;
    }

    private function createTestDeckForOtherUser()
    {
        $this->deleteTestDeckForOtherUser();

        $deck = CustomDeck::create([
            'name'    => $this->testDeck['name'],
            'user_id' => 14,
        ]);

        // remove any lingering associations
        $deck->flashcards()->sync([]);

        foreach ($this->testDeck['flashcard_ids'] as $flashcard_id) {
            if ($flashcard = Flashcard::find($flashcard_id)) {
                $deck->flashcards()->attach($flashcard->id);
            }
        }

        return $deck;
    }

    private function deleteTestDeckForOtherUser()
    {
        $decks = CustomDeck::where('name', $this->testDeck['name'])->where('user_id', 14)->get();

        foreach ($decks as $deck) {
            // remove any lingering associations
            $deck->flashcards()->sync([]);
            $deck->delete();
        }

        return true;
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('custom-decks');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testIndex()
    {
        $this->createTestDeck();
        $this->createTestDeckForOtherUser();

        $token = $this->getToken();
        $response = $this->client->get('custom-decks?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(is_array($json->data));
        $this->assertTrue(count($json->data) > 0);

        foreach ($json->data as $deck) {
            $this->assertEquals($deck->user_id, config('test.id'));
        }

        $this->deleteTestDeck();
        $this->deleteTestDeckForOtherUser();
    }

    public function testShow404()
    {
        $token = $this->getToken();
        $response = $this->client->get('custom-decks/1010101010101?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }

    public function testShow()
    {
        $deck = $this->createTestDeck();

        $token = $this->getToken();
        $response = $this->client->get('custom-decks/' . $deck->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $this->testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name']));
        $this->assertEquals(count($json->data->flashcard_ids), count($this->testDeck['flashcard_ids']));

        $this->deleteTestDeck();
    }

    public function testShowIncorrectUser()
    {
        $deck = $this->createTestDeckForOtherUser();

        $token = $this->getToken();
        $response = $this->client->get('custom-decks/' . $deck->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );

        $this->deleteTestDeckForOtherUser();
    }

    public function testSuccessfulCreateWithArray()
    {
        $this->deleteTestDeck();

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $this->testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $this->testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $this->testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name']));
        $this->assertEquals(count($json->data->flashcard_ids), count($this->testDeck['flashcard_ids']));
        $this->assertEquals($json->data->updated_at, $this->testDeck['updated_at']);
        $this->assertEquals($json->data->created_at, $this->testDeck['created_at']);

        $this->deleteTestDeck();
    }

    public function testSuccessfulCreateWithEmptyFlashcards()
    {
        $deck = $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['flashcard_ids'] = [];

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $this->testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($testDeck['name']));
        $this->assertEquals(count($json->data->flashcard_ids), count($testDeck['flashcard_ids']));
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);

        $this->deleteTestDeck();
    }

    public function testSuccessfulCreateWithMissingFlashcards()
    {
        $deck = $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        unset($testDeck['flashcard_ids']);

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $this->testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($testDeck['name']));
        $this->assertEquals(count($json->data->flashcard_ids), 0);
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);

        $this->deleteTestDeck();
    }

    public function testCreateWithInvalidName()
    {
        $deck = $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        unset($testDeck['name']);

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
        $this->assertTrue(is_array($json->error->name));
        $this->assertTrue(count($json->error->name) > 0);

        $this->deleteTestDeck();
    }

    public function testCreateWithInvalidCreateDate()
    {
        $deck = $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        unset($testDeck['created_at']);

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $this->testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $this->testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name']));
        $this->assertEquals(count($json->data->flashcard_ids), count($this->testDeck['flashcard_ids']));
        $this->assertNotEquals($json->data->created_at, $this->testDeck['created_at']);

        $this->deleteTestDeck();
    }

    public function testSuccessfulDelete()
    {
        $deck = $this->createTestDeck();

        $token = $this->getToken();
        $response = $this->client->delete('custom-decks/' . $deck->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $deletedDeck = CustomDeck::find($deck->id);
        $this->assertNull($deletedDeck);

        $this->deleteTestDeck();
    }

    public function testDeleteWithInvalidInput()
    {
        $deck = $this->createTestDeck();

        $token = $this->getToken();
        $response = $this->client->delete('custom-decks/1010101010101?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );

        $deletedDeck = CustomDeck::find($deck->id);
        $this->assertNotNull($deletedDeck);
        $this->assertEquals($deletedDeck->id, $deck->id);

        $this->deleteTestDeck();
    }

    public function testSuccessfulUpdate()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['name'] = 'Changed Name';
        $testDeck['updated_at'] = '2013-12-01 08:06:21';
        $testDeck['flashcard_ids'] = [100, 300, 500];

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name'])); // slug should not change
        $this->assertEquals(count($json->data->flashcard_ids), count($testDeck['flashcard_ids']));
        $this->assertNotEquals(strtotime($json->data->updated_at), strtotime($this->testDeck['updated_at']));
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));

        $this->deleteTestDeck();
    }

    public function testUpdateWithWrongId()
    {
        $deck = $this->createTestDeck();

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/1010101010101??token=' . urlencode($token), [
            'form_params' => $this->testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $this->deleteTestDeck();
    }

    public function testUpdateWithMissingName()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        unset($testDeck['name']);

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $this->deleteTestDeck();
    }

    public function testSuccessfulUpdateWithEmptyFlashcards()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['flashcard_ids'] = [];

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name'])); // slug should not change
        $this->assertEquals(count($json->data->flashcard_ids), count($testDeck['flashcard_ids']));

        $this->deleteTestDeck();
    }

    public function testSuccessfulUpdateWithMissingFlashcards()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        unset($testDeck['flashcard_ids']);

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $deck = CustomDeck::where('name', $testDeck['name'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->id, $deck->id);
        $this->assertEquals($json->data->name, $testDeck['name']);
        $this->assertEquals($json->data->slug, str_slug($this->testDeck['name'])); // slug should not change
        $this->assertEquals(count($json->data->flashcard_ids), 0);

        $this->deleteTestDeck();
    }

    public function testTimestampsWithCreateBeforeDaylightSavingsTime()
    {
        $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['updated_at'] = '2017-01-20 08:06:21';
        $testDeck['created_at'] = '2017-01-18 09:23:45';

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testDeck['created_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('custom-decks/' . $json->data->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testDeck['created_at']));

        $this->deleteTestDeck();
    }

    public function testTimestampsWithCreateAfterDaylightSavingsTime()
    {
        $this->deleteTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['updated_at'] = '2017-03-25 08:06:21';
        $testDeck['created_at'] = '2017-03-25 06:23:45';

        $token = $this->getToken();
        $response = $this->client->post('custom-decks?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testDeck['created_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('custom-decks/' . $json->data->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));
        $this->assertEquals($json->data->created_at, $testDeck['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testDeck['created_at']));

        $this->deleteTestDeck();
    }

    public function testTimestampswithUpdateBeforeDaylightSavingsTime()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['updated_at'] = '2017-01-20 08:06:21';

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('custom-decks/' . $json->data->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));

        $this->deleteTestDeck();
    }

    public function testTimestampswithUpdateAfterDaylightSavingsTime()
    {
        $deck = $this->createTestDeck();

        $testDeck = $this->testDeck;
        $testDeck['updated_at'] = '2016-03-25 08:06:21';

        $token = $this->getToken();
        $response = $this->client->put('custom-decks/' . $deck->id . '?token=' . urlencode($token), [
            'form_params' => $testDeck,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('custom-decks/' . $json->data->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testDeck['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testDeck['updated_at']));

        $this->deleteTestDeck();
    }
}
