<?php

namespace Tests\Feature;

class CountryTest extends TestCase
{
    public function testDefaults()
    {
        $response = $this->client->get('country');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        $firstItem = $testData[0];
        $this->assertArrayKeys(['code','name'], $firstItem);
    }

    public function test404()
    {
        $response = $this->client->get('country/IDONOTEXIST');

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }
}
