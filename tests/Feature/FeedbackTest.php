<?php

namespace Tests\Feature;

use App\Feedback;

class FeedbackTest extends TestCase
{

    protected $testFeedback = [
        'feedback' => 'Blah Dee Blah',
    ];

    public function testNotAuthenticated()
    {

        $response = $this->client->post('feedback');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testInvalidReasonInput()
    {
        $token = $this->getToken();

        $testFeedback = $this->testFeedback;
        unset($testFeedback['feedback']);

        $response = $this->client->post('feedback?token=' . urlencode($token), [
            'form_params' => $testFeedback,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->feedback));
        $this->assertTrue(count($json->errors->feedback) > 0);
    }

    public function testValidInput()
    {
        $token    = $this->getToken();
        $response = $this->client->post('feedback?token=' . urlencode($token), [
            'form_params' => $this->testFeedback,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $feedback = Feedback::where('feedback', $this->testFeedback['feedback'])->first();

        $this->assertTrue(is_object($feedback));
        $this->assertEquals($feedback->user_id, config('test.id'));
        $this->assertEquals($feedback->feedback, $this->testFeedback['feedback']);

        // clear test report
        Feedback::where('feedback', $this->testFeedback['feedback'])->delete();
    }
}
