<?php

namespace Tests\Feature;

use App\CustomDeck;
use App\DeckState;

class DeckStateTest extends TestCase
{
    protected $testState = [
        'entity_type'      => 'clerkship',
        'entity_specifier' => 1,
        'flashcard_ids'    => '10,11,12,13,14,15,16,17,18,19',
        'index'            => 4,
        'updated_at'       => '2016-12-16 06:27:12',
        'created_at'       => '2016-12-01 10:12:30',
    ];

    private function createTestState()
    {
        $this->deleteTestState();

        return DeckState::create([
            'entity_type'      => $this->testState['entity_type'],
            'entity_specifier' => $this->testState['entity_specifier'],
            'order'            => $this->testState['flashcard_ids'],
            'index'            => $this->testState['index'],
            'user_id'          => config('test.id'),
        ]);
    }

    private function deleteTestState()
    {
        return DeckState::where('user_id', config('test.id'))->delete();
    }

    private function createTestStateForOtherUser()
    {
        $this->deleteTestStateForOtherUser();

        return DeckState::create([
            'entity_type'      => $this->testState['entity_type'],
            'entity_specifier' => $this->testState['entity_specifier'],
            'order'            => $this->testState['flashcard_ids'],
            'index'            => $this->testState['index'],
            'user_id'          => 14,
        ]);
    }

    private function deleteTestStateForOtherUser()
    {
        return DeckState::where('entity_type', $this->testState['entity_type'])
            ->where('entity_specifier', $this->testState['entity_specifier'])
            ->where('user_id', 14)
            ->delete();
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('deck-states');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testSetDeckState()
    {
        // start fresh
        $this->deleteTestState();

        $token    = $this->getToken();
        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $this->testState,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $state = DeckState::where('entity_type', $this->testState['entity_type'])
            ->where('entity_specifier', $this->testState['entity_specifier'])
            ->where('user_id', config('test.id'))
            ->first();

        $this->assertTrue(is_object($state));
        $this->assertEquals($state->user_id, config('test.id'));
        $this->assertEquals($state->order, $this->testState['flashcard_ids']);
        $this->assertEquals($state->index, $this->testState['index']);
        $this->assertEquals($state->updated_at->setTimezone('UTC')->toDateTimeString(), $this->testState['updated_at']);
        $this->assertEquals($state->created_at->setTimezone('UTC')->toDateTimeString(), $this->testState['created_at']);

        // ensure duplicates are not created
        $testState = $this->testState;
        $ids       = $this->testState['flashcard_ids'];
        $ids       = explode(',', $ids);
        shuffle($ids);
        $testState['flashcard_ids'] = implode(',', $ids);
        $testState['index']         = 4;
        $testState['updated_at']    = '2017-01-16 06:27:12';
        $testState['created_at'] > '2017-01-01 10:12:30';

        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $statesQuery = DeckState::where('entity_type', $testState['entity_type'])
            ->where('entity_specifier', $testState['entity_specifier'])
            ->where('user_id', config('test.id'));

        $count = $statesQuery->count();

        $this->assertEquals($count, 1);

        $state = $statesQuery->first();

        $this->assertEquals($state->user_id, config('test.id'));
        $this->assertEquals($state->order, $testState['flashcard_ids']);
        $this->assertEquals($state->index, $testState['index']);
        $this->assertEquals($state->updated_at->setTimezone('UTC')->toDateTimeString(), $testState['updated_at']);
        $this->assertEquals($state->created_at->setTimezone('UTC')->toDateTimeString(), $testState['created_at']);
    }

    public function testSetDeckStateDefaultIndexAndIds()
    {
        $testState = $this->testState;

        $testState['entity_specifier']++;
        unset($testState['flashcard_ids']);
        unset($testState['index']);

        $token    = $this->getToken();
        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $state = DeckState::where('entity_type', $testState['entity_type'])
            ->where('entity_specifier', $testState['entity_specifier'])
            ->where('user_id', config('test.id'))
            ->first();

        $this->assertTrue(is_object($state));
        $this->assertEquals($state->user_id, config('test.id'));
        $this->assertEquals($state->order, '');
        $this->assertEquals($state->index, -1);
    }

    public function testInvalidTypeInput()
    {
        $token = $this->getToken();

        $testState = $this->testState;
        unset($testState['entity_type']);

        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->entity_type));
        $this->assertTrue(count($json->errors->entity_type) > 0);

        $testState['entity_type'] = 'invalid_type';

        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->entity_type));
        $this->assertTrue(count($json->errors->entity_type) > 0);
    }

    public function testInvalidTopicInput()
    {
        $token = $this->getToken();

        $testState                     = $this->testState;
        $testState['entity_specifier'] = 101010101;

        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }

    public function testGetDeckStates()
    {
        $this->createTestState();
        $this->createTestStateForOtherUser();

        $token    = $this->getToken();
        $response = $this->client->get('deck-states?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(is_array($json->data));
        $this->assertTrue(count($json->data) > 0);

        foreach ($json->data as $state) {
            $this->assertEquals($state->user_id, config('test.id'));
        }
    }

    public function testGetFilteredDeckStates()
    {
        $this->createTestStateForOtherUser();
        $this->createTestState();

        $token    = $this->getToken();
        $response = $this->client->get('deck-states?token=' . urlencode($token) . '&entity_type=' . $this->testState['entity_type']);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(is_array($json->data));
        $this->assertTrue(count($json->data) > 0);

        foreach ($json->data as $state) {
            $this->assertEquals($state->entity_type, $this->testState['entity_type']);
            $this->assertEquals($state->user_id, config('test.id'));
        }

        $this->deleteTestState();
    }

    public function testGetSingleDeckState()
    {
        $this->createTestState();

        $token    = $this->getToken();
        $response = $this->client->get('deck-states?token=' . urlencode($token) . '&entity_type=' . $this->testState['entity_type'] . '&entity_specifier=' . $this->testState['entity_specifier'] . '');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->entity_specifier, $this->testState['entity_specifier']);
        $this->assertEquals($json->data->entity_type, $this->testState['entity_type']);
        $this->assertEquals($json->data->flashcard_ids, $this->testState['flashcard_ids']);
        $this->assertEquals($json->data->index, $this->testState['index']);
    }

    public function testGetSingleDeckStateForOtherUser()
    {
        $this->createTestState();

        $token    = $this->getToken();
        $response = $this->client->get('deck-states?token=' . urlencode($token) . '&entity_type=' . $this->testState['entity_type'] . '&entity_specifier=' . $this->testState['entity_specifier'] . '');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->entity_specifier, $this->testState['entity_specifier']);
        $this->assertEquals($json->data->entity_type, $this->testState['entity_type']);
        $this->assertEquals($json->data->flashcard_ids, $this->testState['flashcard_ids']);
        $this->assertEquals($json->data->index, $this->testState['index']);

        $this->deleteTestState();
    }

    public function testSetDeckStateForEachType()
    {
        $testState = $this->testState;

        $validTypes = [
            'topic',
            'category',
            'clerkship',
            'user_deck',
            'system_deck',
        ];

        foreach ($validTypes as $type) {

            $testState['entity_type'] = $type;

            if ($type == 'user_deck') {
                $deck = CustomDeck::create([
                    'name'    => 'Testing',
                    'user_id' => config('test.id'),
                ]);

                $testState['entity_specifier'] = $deck->id;
            } else {
                $testState['entity_specifier'] = 1;
            }

            $token    = $this->getToken();
            $response = $this->client->post('deck-states?token=' . urlencode($token), [
                'form_params' => $testState,
            ]);

            $this->assertEquals(
                self::HTTP_OK,
                $response->getStatusCode()
            );

            $state = DeckState::where('entity_type', $testState['entity_type'])
                ->where('entity_specifier', $testState['entity_specifier'])
                ->where('user_id', config('test.id'))
                ->first();

            $this->assertTrue(is_object($state));
            $this->assertEquals($state->user_id, config('test.id'));
            $this->assertEquals($state->order, $testState['flashcard_ids']);
            $this->assertEquals($state->index, $testState['index']);
        }

        $this->deleteTestState();
    }

    public function testTimestampsBeforeDaylightSavingsTime()
    {
        // start fresh
        $this->deleteTestState();

        $testState               = $this->testState;
        $testState['updated_at'] = '2017-01-20 08:06:21';
        $testState['created_at'] = '2017-01-18 09:23:45';

        $token    = $this->getToken();
        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());
        $this->assertTrue(property_exists($json, 'message'));

        $response = $this->client->get('deck-states?token=' . urlencode($token) . '&entity_type=' . $testState['entity_type'] . '&entity_specifier=' . $testState['entity_specifier'] . '');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->user_id, config('test.id'));
        $this->assertEquals($json->data->updated_at, $testState['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testState['updated_at']));
        $this->assertEquals($json->data->created_at, $testState['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testState['created_at']));
    }

    public function testTimestampsAfterDaylightSavingsTime()
    {
        // start fresh
        $this->deleteTestState();

        $testState               = $this->testState;
        $testState['updated_at'] = '2017-03-25 08:06:21';
        $testState['created_at'] = '2017-03-25 06:23:45';

        $token    = $this->getToken();
        $response = $this->client->post('deck-states?token=' . urlencode($token), [
            'form_params' => $testState,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());
        $this->assertTrue(property_exists($json, 'message'));

        $response = $this->client->get('deck-states?token=' . urlencode($token) . '&entity_type=' . $testState['entity_type'] . '&entity_specifier=' . $testState['entity_specifier'] . '');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->user_id, config('test.id'));
        $this->assertEquals($json->data->updated_at, $testState['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testState['updated_at']));
        $this->assertEquals($json->data->created_at, $testState['created_at']);
        $this->assertEquals(strtotime($json->data->created_at), strtotime($testState['created_at']));
    }
}
