<?php

namespace Tests\Feature;

class ClerkshipsTest extends TestCase
{
    public function testNotAuthenticated()
    {
        $response = $this->client->get('clerkships');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testIndex()
    {
        $token    = $this->getToken();
        $response = $this->client->get('clerkships?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(is_array($json->data));
        $this->assertTrue(count($json->data) > 0);
    }

}
