<?php

namespace Tests\Feature;

use App\User;
use Carbon\Carbon;

class RegistrationTest extends TestCase
{
    /**
     * Tests for Signup Endpoint
     */
    public function testEmptySignupRequest()
    {
        $response = $this->client->post('signup');

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testMissingSignupRequestData()
    {
        $response = $this->client->post('signup', [
            'form_params' => [
                'email'     => config('test.username'),
                'password'  => config('test.password'),
            ],
        ]);

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testBadSignupRequestData()
    {
        $response = $this->client->post('signup', [
            'form_params' => [
                'first_name'            => 'Isaac',
                'last_name'             => 'Asimov',
                'email'                 => 'Isaac@Asmiov.com',
                'password'              => 'trantor',
                'password_confirmation' => 'trantor',
                'user_type'             => 'author',
                'plan'                  => 'psychohistory',
            ],
        ]);

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGoodSignupRequestData()
    {
        $testEmail = 'IsaacAsimov@signup-test.com';
        User::where('email', $testEmail)->delete();
        $response = $this->client->post('signup', [
            'form_params' => [
                'profession_id'         => 1,
                'year'                  => Carbon::now()->addYear(2)->year,
                'school_text_id'        => 1,
                'first_name'            => 'Isaac',
                'last_name'             => 'Asimov',
                'email'                 => $testEmail,
                'password'              => 'trantor',
                'password_confirmation' => 'trantor',
                'user_type'             => 'professional',
                'plan'                  => 'free',
            ],
        ]);
        $json = json_decode($response->getBody());
        User::where('email', $testEmail)->delete();

        $this->assertEquals(self::HTTP_OK, $response->getStatusCode());

        $this->assertTrue(property_exists($json, 'token'));
        $this->assertEquals($testEmail, $json->data->email);
        $this->assertTrue(property_exists($json, 'data'));
    }

    /**
     * Tests for Password Reset Endpoint
     */
    public function testEmptyPasswordResetRequest()
    {
        $response = $this->client->post('password-reset');

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testMissingPasswordResetRequestData()
    {
        $response = $this->client->post('password-reset', [
            'form_params' => [
                'email'     => config('test.username'),
                'password'  => config('test.password'),
            ],
        ]);

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testBadPasswordResetRequestData()
    {
        $response = $this->client->post('password-reset', [
            'form_params' => [
                'email'                 => 'Han Solo',
                'password'              => config('test.password'),
                'password_confirmation' => config('test.password'),
            ],
        ]);

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGoodPasswordResetRequestData()
    {
        $rand = '-test-' . rand(100, 999);

        $response = $this->client->post('password-reset', [
            'form_params' => [
                'email'                 => config('test.username'),
                'password'              => config('test.password') . $rand,
                'password_confirmation' => config('test.password') . $rand,
            ],
        ]);

        $this->assertEquals(self::HTTP_OK, $response->getStatusCode());

        $json = json_decode($response->getBody());
        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(property_exists($json, 'token'));

        // Revert to the original password
        $user = User::find(config('test.id'));
        $user->password = config('test.password');
        $user->save();
    }

    /**
     * Tests for Forgotten Password Endpoint
     */
    public function testEmptyPasswordForgotRequest()
    {
        $response = $this->client->post('password-forgot');

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testBadPasswordForgotRequestData()
    {
        $response = $this->client->post('password-forgot', [
            'form_params' => [
                'email' => 'Luke Skywalker',
            ],
        ]);

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGoodPasswordForgotRequestData()
    {
        $response = $this->client->post('password-forgot', [
            'form_params' => [
                'email' => config('test.username'),
            ],
        ]);

        $this->assertEquals(self::HTTP_OK, $response->getStatusCode());

        $json = json_decode($response->getBody());
        $this->assertTrue(property_exists($json, 'message'));
    }
}