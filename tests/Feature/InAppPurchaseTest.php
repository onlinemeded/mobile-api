<?php

namespace Tests\Feature;

use ReceiptValidator\iTunes\Validator as iTunesValidator;
use ReceiptValidator\GooglePlay\Validator as PlayValidator;
use App\Http\Controllers\MicroTransactionController;

class InAppPurchaseTest extends TestCase
{

    // the receipt/purchase_token and product_id must match
    private const IOS_BASE64_TEST_RECEIPT = 'MIIVMAYJKoZIhvcNAQcCoIIVITCCFR0CAQExCzAJBgUrDgMCGgUAMIIE0QYJKoZIhvcNAQcBoIIEwgSCBL4xggS6MAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATEwCwIBCwIBAQQDAgEAMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAijANAgENAgEBBAUCAwHVKDANAgETAgEBBAUMAzEuMDAOAgEJAgEBBAYCBFAyNTMwGAIBBAIBAgQQAj8VwGYjXM6pgjq5uAR4YDAbAgEAAgEBBBMMEVByb2R1Y3Rpb25TYW5kYm94MBwCAQUCAQEEFJQjuPbK2bDPSZxLxjp5ThUUO3xLMB4CAQwCAQEEFhYUMjAxOS0wOC0wN1QxNDowNzo1MVowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjAjAgECAgEBBBsMGWNvbS5vbmxpbmVtZWRlZC5tb2JpbGVvbWUwOwIBBwIBAQQzLG3grEdN/tf1c3rJTUwIN+v43AJbT810oeSVL0NEuImUinJ0fx80dOGJpvdzjyqKiTHAMFwCAQYCAQEEVMVFb49F/gC8GxkdNFuOGxKC3m28jUbGoJg7LPgz4fAaMBIA7jmD/qhabA3jXV35Wd8BLsGfLcjuvlzzzAKX/3iEbLeXv7FpiFwWV7bapwfPx+yCizCCAVQCARECAQEEggFKMYIBRjALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBADAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAaAgIGpgIBAQQRDA9pbnRlcm5fYm9vdGNhbXAwGwICBqcCAQEEEgwQMTAwMDAwMDU1NTM0MTU2MDAbAgIGqQIBAQQSDBAxMDAwMDAwNTU1MzQxNTYwMB8CAgaoAgEBBBYWFDIwMTktMDgtMDdUMTQ6MDc6NTFaMB8CAgaqAgEBBBYWFDIwMTktMDgtMDdUMTQ6MDc6NTFaMIIBWQIBEQIBAQSCAU8xggFLMAsCAgasAgEBBAIWADALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEAMAwCAgauAgEBBAMCAQAwDAICBq8CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBsCAganAgEBBBIMEDEwMDAwMDA1NTUzMzI4NTUwGwICBqkCAQEEEgwQMTAwMDAwMDU1NTMzMjg1NTAfAgIGpgIBAQQWDBRxdWlja190YWJsZXNfZGlnaXRhbDAfAgIGqAIBAQQWFhQyMDE5LTA4LTA3VDEzOjQ5OjExWjAfAgIGqgIBAQQWFhQyMDE5LTA4LTA3VDEzOjQ5OjExWqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAodrtn5D7fiVAP9Z0MxWraSGGWff45kVV0KUr7g2d5jNrR7ZGb/UnvkKW3xa190xw01IZTM/EmiLYJ/+F78xutNGqc8shS0JBeOr5O6QIDwa+dLKOSQcHDgztktsqwchzkz2YMH4digdjwxO2/1+eM8YnDT7LHlGsMJ4IlAT1Ow0TSWFZ/Y574Mqzwxyf41r8nB4C4/dawarxASD93zcfkUojICNW5pDzqkOOoJ2A8wHC7T+SW9fVFm7VPkNxVzXJJcJfZ3h31Iy28fD30vPbH5w0KQbwTUWM7tJuaRjBp8BkGc0HvDCifqEXCc2vaNFGWOlJTJSr3aI0Z4Y3Ux3q5A==';
    private const OME_PRODUCT_ID = 'quick_tables_digital';
    private const ANDROID_PURCHASE_TOKEN_QUICK_TABLES_DIGITAL = 'apnipldbnhbdldcbafkejhdg.AO-J1OyF4Z-ZfPnxGRR4vUivn1OWrwzCXG9yDaPo1tBmvruWSEniXUBHSM1VQDVKsqKemIrKbubq_SB3LlBDP7nfnsB63ZoWX36IQPJ9901xZot-MJBWNn1_YjoggmLKwWEYM3Gq1r1R';
    private const ANDROID_ORDER_ID = 'GPA.3358-4899-6581-39483';

    public function testConfirmReceiptIos()
    {
        $validator = new iTunesValidator(iTunesValidator::ENDPOINT_SANDBOX);
        $receiptBase64Data = self::IOS_BASE64_TEST_RECEIPT;

        $response = $validator->setReceiptData($receiptBase64Data)->validate();

        self::assertTrue($response->isValid());
        self::assertCount(1, $response->getPurchases());
        $productId = $response->getPurchases()[0]->getProductId();
        self::assertEquals('com.mindmobapp.download', $productId);
    }

    public function testConfirmReceiptAndroid()
    {
        $keyFilePath = MicroTransactionController::googleServiceAccountFilePath();
        self::assertFileIsReadable($keyFilePath);

        $googleClient = new \Google_Client();
        $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setApplicationName(MicroTransactionController::ANDROID_PURCHASE_VALIDATOR_NAME);
        $googleClient->setAuthConfig($keyFilePath);

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($googleClient);
        $validator = new PlayValidator($googleAndroidPublisher);

        $response = null;
        $error = null;
        try {
            // any validation failure on a valid product throws an exception with a code 500
            $response = $validator->setPackageName(MicroTransactionController::ANDROID_APP_PACKAGE_NAME)
                ->setProductId(self::OME_PRODUCT_ID)
                ->setPurchaseToken(self::ANDROID_PURCHASE_TOKEN_QUICK_TABLES_DIGITAL)
                ->validatePurchase();
        } catch (\Exception $e) {
            // example messages:
            //  Error calling GET ....: (404) Product not found for this application.
            $error = $e;
            // var_dump($e->getMessage());
        }

        /* expected good response
        {
         "kind": "androidpublisher#inappPurchase",
         "purchaseTime": "1562770842755",
         "purchaseState": 0,
         "consumptionState": 0,
         "developerPayload": "",
         "orderId": "GPA.3391-9047-4310-29194"
        }
        */

        // avoid assertNull because it produces too much output
        self::assertEquals(null, $error);
        self::assertEquals(self::ANDROID_ORDER_ID, $response->getRawResponse()['orderId']);
    }

    public function testPostInAppPurchaseIos()
    {
        $token   = $this->getToken();

        $response = $this->client->post('iap', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => [
                'sku' => self::OME_PRODUCT_ID,
                'receipt' => self::IOS_BASE64_TEST_RECEIPT,
                'platform' => 'ios',
            ]
        ]);
        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
    }

    public function testPostInAppPurchaseAndroid()
    {
        $token   = $this->getToken();

        $response = $this->client->post('iap', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => [
                'sku' => self::OME_PRODUCT_ID,
                'receipt' => self::ANDROID_PURCHASE_TOKEN_QUICK_TABLES_DIGITAL,
                'platform' => 'android',
            ]
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
    }
}
