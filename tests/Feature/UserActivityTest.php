<?php

namespace Tests\Feature;

use App\UserMetric;
use App\User;

class UserActivityTest extends TestCase
{
    private function createTestMetrics($metricType, $metricCount = 1)
    {
        $user = User::find(config('test.id'));
        $this->deleteTestMetrics();

        for ($i = 0; $i < $metricCount; $i++) {
            $topic = $user->careerTrack->getTopics()->get()->random();

            UserMetric::create([
                'user_id'       => config('test.id'),
                'type'          => $metricType,
                'object_id'     => $topic->id,
                'is_remediation' => false,
                'ip'            => '127.0.0.1',
                'description'   => 'TEST METRIC',
            ]);
        }
    }

    private function deleteTestMetrics()
    {
        UserMetric::where('user_id', config('test.id'))->delete();
    }

    public function testGetVideoActivity()
    {
        $metricCount = rand(0, 20);
        $this->createTestMetrics('video', $metricCount);

        $token = $this->getToken();
        $response = $this->client->get('user/activity/video?token=' . urlencode($token));
        $json = json_decode($response->getBody());

        $this->deleteTestMetrics();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        // api returns a max of 5 metrics (recent activity)
        $this->assertCount(min(5, $metricCount), $json->data);
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(0, count($json->data));

        $expectedKeys = [
            'id',
            'user_id',
            'object_id',
            'is_remediation',
            'type',
            'description',
            'value',
            'ip',
            'topic',
            'created_at',
            'updated_at'];
        $firstItem = $json->data[0];
        foreach ($expectedKeys as $key) {
            $this->assertObjectHasAttribute($key, $firstItem);
        }
        $this->assertEquals(config('test.id'), $firstItem->user_id);
    }

    public function testQBankResults()
    {
        // TODO: mock qbank and results data
        $token = $this->getToken();
        $response = $this->client->get('user/activity/qbank/summary?token=' . urlencode($token));
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertObjectHasAttribute('summary', $json);
        $this->assertObjectHasAttribute('qbank', $json->summary);
        $this->assertObjectHasAttribute('total', $json->summary->qbank);
        $this->assertObjectHasAttribute('correct', $json->summary->qbank);
        $this->assertObjectHasAttribute('wrong', $json->summary->qbank);
        $this->assertObjectHasAttribute('bookmarked', $json->summary->qbank);
        $this->assertObjectHasAttribute('questions', $json->summary->qbank);
    }
}
