<?php

namespace Tests\Feature;

class InstitutionTest extends TestCase
{
    public function testDefaults()
    {
        $response = $this->client->get('institutions');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        //print_r($testData);
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        $firstItem = $testData[0];
        $this->assertArrayKeys(['id','slug','name'], $firstItem);
    }

    public function testSelectedColumns()
    {
        $response = $this->client->get('institutions?select=id,nickname');
        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertInternalType('array', $testData);
        $this->assertGreaterThan(0, count($testData));
        $firstItem = $testData[0];
        $this->assertArrayKeys(['id', 'slug', 'name'], $firstItem);
    }

    public function testCountrySearch()
    {
        $everythingResponse = $this->client->get('institutions');
        $ugandaResponse = $this->client->get('institutions?country=Uganda');
        $this->assertEquals(
            self::HTTP_OK,
            $everythingResponse->getStatusCode()
        );
        $this->assertEquals(
            self::HTTP_OK,
            $ugandaResponse->getStatusCode()
        );
        $allRowsRes = json_decode($everythingResponse->getBody(), true);
        $ugandanRowsRes = json_decode($ugandaResponse->getBody(), true);
        $allRows = $allRowsRes['data'];
        $ugandanRows = $ugandanRowsRes['data'];
        //print_r($testData);
        $this->assertTrue(is_array($allRows));
        $this->assertTrue(is_array($ugandanRows));
        $this->assertTrue(count($allRows) > count($ugandanRows));
    }


    public function test404()
    {
        $response = $this->client->get('institutions/IDONOTEXIST');

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }
}
