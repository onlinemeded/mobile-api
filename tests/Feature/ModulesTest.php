<?php

namespace Tests\Feature;

use App\Module;
use App\QBankQuestion;
use App\User;

// Expected response json shapes
const RESULT_SHAPE = [
    'qbank_id',
    'result',
    'answer',
    'is_correct'
];

const STATS_SHAPE = [
    'answertotal',
    'answercorrect',
    'Answer1',
    'Answer2',
    'Answer3',
    'Answer4',
    'Answer5',
    'Answer6',
    'Answer7',
    'Answer8',
];

const QUESTION_SHAPE = [
    'id',
    'linked_id',
    'category_id',
    'topic_id',
    'physician_tasks',
    'vignette',
    'interrogatory',
    'Answer1',
    'Answer2',
    'Answer3',
    'Answer4',
    'Answer5',
    'Answer6',
    'Answer7',
    'Answer8',
    'correctanswer',
    'explanation',
    'teachingpoint',
    'source',
    'status',
    'is_bookmarked',
    'result', // null or RESULT_SHAPE
    'stats' => STATS_SHAPE,
];

const MODULE_SHAPE = [
    'id',
    'user_id',
    'display_name',
    'questionids',
    'testmode',
    'numquestions',
    'numcorrect',
    'complete',
    'starttime',
    'timerlength',
    'finishtime',
    'active',
    'answer_attempts',
    'questions' => [QUESTION_SHAPE],
];

class ModulesTest extends TestCase
{
    protected $base_route = 'qbank/module';


    private function randomQuestionIdsInCareerTrack($userId) {
        $user = User::find($userId);
        $user->career_track_id;
        $topicIds = $user->careerTrack->getTopics()->pluck('id')->toArray();
        $allQuestionIds = QBankQuestion::whereIn('topic_id', $topicIds)
            ->pluck('id')->toArray();
        $numQuestions = rand(1, count($allQuestionIds));
        shuffle($allQuestionIds);
        return array_slice($allQuestionIds, 0, $numQuestions);
    }

    private function createTestModule($userId = null) {
        $userId = $userId ?: config('test.id');
        $questionIds = $this->randomQuestionIdsInCareerTrack($userId);
        $module = Module::create([
            'settings->display_name' => 'UNIT TEST MODULE',
            'settings->active'       => true,
            'user_id'                => $userId,
            'testmode'               => 'timed',
            'questionmode'           => 'all',
            'questionids'            => implode($questionIds, ','),
            "timerlength"            => 60,
        ]);
        return $module;
    }

    private function deleteTestModules()
    {
        Module::where('user_id', config('test.id'))->delete();
    }

    /**
     * GET qbank/question Endpoint
     */
    public function testNotAuthenticated()
    {
        $response = $this->client->get($this->base_route);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testGetModule()
    {
        $module = $this->createTestModule();
        $token = $this->getToken();

        $url = config('app.url') . '/api/v1/qbank/module/' . $module->id;

        $response = $this->getJson(
            $url,
            ['Authorization' => 'Bearer ' . $token]
        )->assertStatus(self::HTTP_OK);

        $this->deleteTestModules();

        $response->assertJsonStructure([
            'data' => MODULE_SHAPE
        ]);

        $json = json_decode($response->getContent());
        $question = $json->data->questions[0];
        $stats = $question->stats;
        $this->assertInternalType('int', $stats->answertotal);
        $this->assertInternalType('int', $stats->Answer1);
    }

    public function testGetAllModules()
    {
        $this->createTestModule();
        $token = $this->getToken();
        $response = $this->client->get($this->base_route . '?token=' . urlencode($token));
        $json = json_decode($response->getBody());
        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(0, count($json->data));
    }

    /**
     * POST qbank/module/ Endpoint
     */
    public function testCreateModule()
    {
        $max_allowed_questions = 10;
        $userId = config('test.id');
        $token = $this->getToken();

        $url = config('app.url') . '/api/v1/qbank/module';

        $response = $this->postJson(
            $url,
            ['user_id'        => $userId,
             'mode'           => 'timed',
             'question_types' => ['all' => true],
             'categories'     => ['all' => true],
             'max_allowed_questions' => $max_allowed_questions,
             'display_name'   => 'UNIT TESTING',
            ],
            ['Authorization' => 'Bearer ' . $token]
        )->assertStatus(self::HTTP_OK);

        $response->assertJsonStructure([
            'data' => MODULE_SHAPE
        ]);

        $json = json_decode($response->getContent());
        $this->assertEquals($json->data->testmode, 'timed');
        $this->assertLessThanOrEqual($max_allowed_questions, count($json->data->questions));
        $expected_timer_length = floor((count($json->data->questions) * Module::TIMED_TEST_SECONDS_PER_QUESTION) / 60);
        $this->assertEquals($expected_timer_length, $json->data->timerlength);

        Module::findOrFail($json->data->id)->delete();
    }

    public function testCreateModuleMissingData()
    {
        $token   = $this->getToken();

        $response = $this->client->post($this->base_route, [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => [
                'mode'      => 'unit_testing',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    /**
     * PUT qbank/module/1 Endpoint
     */
    public function testUpdateModule()
    {
        $module = $this->createTestModule();

        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id, [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => [
                'questionmode' => 'unit_testing_update',
            ],
        ]);
        $json = json_decode($response->getBody());

        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals('unit_testing_update', $json->data->questionmode);
    }

    public function testUpdateNonExistingModule()
    {
        $token = $this->getToken();
        $module_id = 1234567891011121314151617;

        $response = $this->client->put($this->base_route . '/' . $module_id, [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => [
                'questionmode'      => 'unit_testing',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }

    /**
     * PUT qbank/module/1/start
     */
    public function testStartModule() {
        $module = $this->createTestModule();

        $starttime = time();
        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id . '/start', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['starttime' => $starttime]
        ]);
        $json = json_decode($response->getBody());

        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals(0, $json->data->complete);
        $this->assertEquals($starttime, $json->data->starttime);
    }

    /**
     * PUT qbank/module/1/finish
     */
    public function testFinishModule() {
        $module = $this->createTestModule();
        $module->starttime = time() - 60*60;
        $module->update();

        $finishtime = time();
        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id . '/finish', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['finishtime' => $finishtime]
        ]);
        $json = json_decode($response->getBody());

        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals(1, $json->data->complete);
        $this->assertEquals($finishtime, $json->data->finishtime);
    }

    public function testFinishModuleThatHasNotStarted() {
        $module = $this->createTestModule();

        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id . '/finish', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['finishtime' => 123]
        ]);
        $json = json_decode($response->getBody());

        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'error'));
    }

    public function testFinishModuleWithInvalidTime() {
        $module = $this->createTestModule();
        $module->starttime = time() - 60*60;
        $module->update();

        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id . '/finish', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['finishtime' => 123]
        ]);
        $json = json_decode($response->getBody());

        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'error'));
    }

    public function testFinishModuleWithoutTime() {
        $module = $this->createTestModule();
        $module->starttime = time() - 60*60;
        $module->update();

        $token = $this->getToken();
        $response = $this->client->put($this->base_route . '/' . $module->id . '/finish', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
        ]);
        $json = json_decode($response->getBody());
        $this->deleteTestModules();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $this->assertTrue(property_exists($json->data, 'finishtime'));
        $this->assertGreaterThan(0, $json->data->finishtime);
    }
}
