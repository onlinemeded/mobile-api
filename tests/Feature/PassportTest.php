<?php

namespace Tests\Feature;

use App\ApiKey;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;

class PassportTest extends TestCase
{
    use DatabaseTransactions;

    protected $connectionsToTransact = [
        'mysql',
    ];

    /** @test */
    public function it_should_successfully_connect_to_ome_main_with_passport()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => config('main.url')
        ]);
        $user = User::inRandomOrder()->first();

        $response = $client->post('api/v1/user/' . $user->id . '/download-limit', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . ApiKey::where('property_id', 'flashback-api')->first()->access_token
            ]
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
