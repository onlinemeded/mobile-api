<?php

namespace Tests\Feature;

use App\User;

class CategoriesTest extends TestCase
{
    public function testNotAuthenticated()
    {
        $response = $this->client->get('categoriesWithStats');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testIndexCareerTrackClinical()
    {
        $user = User::find(config('test.id'));
        $origTrack = $user->career_track_id;
        $user->career_track_id = 2;
        $user->save();

        $token = $this->getToken();
        $response = $this->client->get('categoriesWithStats?token=' . urlencode($token));

        $user->career_track_id = $origTrack;
        $user->save();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(0, count($json->data));
        $category = $json->data[0];
        $this->assertInternalType('array', $category->taglines);
        $this->assertGreaterThan(0, count($category->taglines));
    }

    public function testIndexCareerTrackBasicSciences()
    {
        $user = User::find(config('test.id'));
        $origTrack = $user->career_track_id;
        $user->career_track_id = 1;
        $user->save();

        $token = $this->getToken();
        $response = $this->client->get('categoriesWithStats?token=' . urlencode($token));

        $user->career_track_id = $origTrack;
        $user->save();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(0, count($json->data));
    }
}
