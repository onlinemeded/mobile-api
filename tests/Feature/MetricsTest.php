<?php

namespace Tests\Feature;

use App\Topic;
use App\TopicHistory;
use App\User;
use Carbon\Carbon;

class MetricsTest extends TestCase
{

    private function addFreshRecentTopic($topicId = null)
    {
        if ($topicId === null) {
            $user = User::find(config('test.id'));
            $topic = $user->careerTrack->getTopics()->get()->random();
        } else {
            $topic = Topic::find($topicId);
        }

        return TopicHistory::create([
            'topic_id' => $topic->id,
            'user_id'  => config('test.id'),
            'time'     => Carbon::now()->toDateTimeString(),
        ]);
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('metrics/recent-topics');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testGetRecentTopics()
    {
        $this->addFreshRecentTopic();

        $token = $this->getToken();
        $response = $this->client->get('metrics/recent-topics?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(1, count($json->data));

        $firstMetric = current($json->data);
        $lastMetric  = end($json->data);

        $this->assertEquals(config('test.id'), $firstMetric->user_id);
        $this->assertEquals(config('test.id'), $lastMetric->user_id);

        $this->assertTrue(strtotime($firstMetric->last_viewed_at) > strtotime($lastMetric->last_viewed_at));
        $this->assertNull($firstMetric->last_synced_at);
        $this->assertNull($firstMetric->reviewed_at);
        $this->assertNull($firstMetric->next_due_at);
    }

    public function testPostRecentTopicsBeforeDST()
    {
        $firstMetricInitial = $this->addFreshRecentTopic();

        $this->assertNotNull($firstMetricInitial);

        $newValues = [
            'last_synced_at'         => '2017-01-20 13:12:10',
            'reviewed_at'            => '2017-01-19 11:56:12',
            'next_due_at'            => '2017-02-02 08:06:21',
            'next_due_interval_days' => 14,
        ];

        $token    = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => $newValues,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));

        $this->assertEquals($json->data->user_id, $firstMetricInitial->user_id);
        $this->assertEquals($json->data->topic_id, $firstMetricInitial->topic_id);
        $this->assertEquals($json->data->last_synced_at, $newValues['last_synced_at']);
        $this->assertEquals($json->data->reviewed_at, $newValues['reviewed_at']);
        $this->assertEquals($json->data->next_due_at, $newValues['next_due_at']);
        $this->assertEquals($json->data->next_due_interval_days, $newValues['next_due_interval_days']);
    }

    public function testPostRecentTopicsAfterDST()
    {
        $firstMetricInitial = $this->addFreshRecentTopic();

        $this->assertNotNull($firstMetricInitial);

        $newValues = [
            'last_synced_at'         => '2017-04-02 13:12:10',
            'reviewed_at'            => '2017-04-01 11:56:12',
            'next_due_at'            => '2017-04-08 08:06:21',
            'next_due_interval_days' => 7,
        ];

        $token    = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => $newValues,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));

        $this->assertEquals($json->data->user_id, $firstMetricInitial->user_id);
        $this->assertEquals($json->data->topic_id, $firstMetricInitial->topic_id);
        $this->assertEquals($json->data->last_synced_at, $newValues['last_synced_at']);
        $this->assertEquals($json->data->reviewed_at, $newValues['reviewed_at']);
        $this->assertEquals($json->data->next_due_at, $newValues['next_due_at']);
        $this->assertEquals($json->data->next_due_interval_days, $newValues['next_due_interval_days']);
    }

    public function testFetchGroupedTopic()
    {
        // ensure we have two history items with the same topic id
        $user = User::find(config('test.id'));
        $topicId = $user->careerTrack->getTopics()->pluck('id')->random();

        $firstMetricInitial = $this->addFreshRecentTopic($topicId);
        sleep(2); // must have at least one second between them
        $secondMetricInitial = $this->addFreshRecentTopic($topicId);

        $this->assertNotNull($firstMetricInitial);
        $this->assertNotNull($secondMetricInitial);

        $newValues = [
            'next_due_interval_days' => 31,
        ];

        // set a value on the most recent topic
        $token = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $topicId . '?token=' . urlencode($token), [
            'form_params' => $newValues,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        // now fetch the topics
        $response = $this->client->get('metrics/recent-topics?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(1, count($json->data));

        $firstMetric = current($json->data);

        $this->assertEquals(config('test.id'), $firstMetric->user_id);
        $this->assertEquals($newValues['next_due_interval_days'], $firstMetric->next_due_interval_days);

        // ensure we don't have duplicate topics returned
        $idCount = 0;
        reset($json->data);
        foreach ($json->data as $item) {
            if ($item->topic_id == $topicId) {
                $idCount++;
            }
        }

        $this->assertEquals(1, $idCount);
    }

    public function testPostMissingTopic()
    {
        $token    = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/555555555555555?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }

    public function testPostNoParams()
    {
        $firstMetricInitial = $this->addFreshRecentTopic();

        $this->assertNotNull($firstMetricInitial);

        $token    = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));

        $this->assertEquals($json->data->user_id, $firstMetricInitial->user_id);
        $this->assertEquals($json->data->topic_id, $firstMetricInitial->topic_id);
        $this->assertNull($json->data->last_synced_at);
        $this->assertNull($json->data->reviewed_at);
        $this->assertNull($json->data->next_due_at);
        $this->assertEquals($json->data->next_due_interval_days, 0);
    }

    public function testPostEmptyParams()
    {
        $firstMetricInitial = $this->addFreshRecentTopic();

        $this->assertNotNull($firstMetricInitial);

        $token    = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'last_synced_at'         => '',
            'reviewed_at'            => '',
            'next_due_at'            => '',
            'next_due_interval_days' => 0,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));

        $this->assertEquals($json->data->user_id, $firstMetricInitial->user_id);
        $this->assertEquals($json->data->topic_id, $firstMetricInitial->topic_id);
        $this->assertNull($json->data->last_synced_at);
        $this->assertNull($json->data->reviewed_at);
        $this->assertNull($json->data->next_due_at);
        $this->assertEquals($json->data->next_due_interval_days, 0);
    }

    public function testPostInvalidParams()
    {
        $firstMetricInitial = $this->addFreshRecentTopic();

        $this->assertNotNull($firstMetricInitial);

        $token = $this->getToken();
        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => ['last_synced_at' => 'xxx'],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
        $this->assertInternalType('array', $json->error->last_synced_at);
        $this->assertGreaterThan(0, count($json->error->last_synced_at));

        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => ['reviewed_at' => 'xxx'],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
        $this->assertInternalType('array', $json->error->reviewed_at);
        $this->assertGreaterThan(0, count($json->error->reviewed_at));

        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => ['next_due_at' => 'xxx'],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
        $this->assertInternalType('array', $json->error->next_due_at);
        $this->assertGreaterThan(0, count($json->error->next_due_at));

        $response = $this->client->post('metrics/recent-topics/' . $firstMetricInitial->topic_id . '?token=' . urlencode($token), [
            'form_params' => ['next_due_interval_days' => 'xxx'],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
        $this->assertInternalType('array', $json->error->next_due_interval_days);
        $this->assertGreaterThan(0, count($json->error->next_due_interval_days));
    }

    public function testStudiedTopics()
    {
        $this->addFreshRecentTopic();

        $token = $this->getToken();
        $response = $this->client->get('metrics/studied-topics?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        // TODO: there's no reason to expect any data here, is part of the test missing?
        /*
        $this->assertGreaterThan(1, count($json->data));

        $firstMetric = current($json->data);
        $lastMetric  = end($json->data);

        $this->assertEquals(config('test.id'), $firstMetric->user_id);
        $this->assertEquals(config('test.id'), $lastMetric->user_id);

        $this->assertTrue(strtotime($firstMetric->studied_at) > strtotime($lastMetric->studied_at));
        */
    }
}
