<?php

namespace App\Transformers;

use App\SystemDeckType;
use App\Transformers\Transformer;

class SystemDeckTypeTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(SystemDeckType $deck)
    {
        return [
            'id'              => (int) $deck->id,
            'name'            => $deck->name,
            'slug'            => $deck->slug,
            'is_configurable' => $deck->is_configurable == 1,
            'created_at'      => $this->convertServerTimeToUTC($deck->created_at),
            'updated_at'      => $this->convertServerTimeToUTC($deck->updated_at),
        ];
    }
}
