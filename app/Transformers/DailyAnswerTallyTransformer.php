<?php

namespace App\Transformers;

use App\DailyAnswerTally;
use App\Transformers\Transformer;

class DailyAnswerTallyTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(DailyAnswerTally $tally)
    {
        return [
            'todays_total'   => (int) $tally->total,
            'all_time_total' => (int) $tally->getHighAnswerTally(),
            'flipped_on'     => $tally->flipped_on->toDateString(),
            'alert_shown_at' => $this->convertServerTimeToUTC($tally->alert_shown_at),
            'updated_at'     => $this->convertServerTimeToUTC($tally->updated_at),
        ];
    }
}
