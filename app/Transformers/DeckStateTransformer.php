<?php

namespace App\Transformers;

use App\DeckState;
use App\Transformers\Transformer;

class DeckStateTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(DeckState $state)
    {
        return [
            'user_id'          => (int) $state->user_id,
            'entity_type'      => $state->entity_type,
            'entity_specifier' => (int) $state->entity_specifier,
            'flashcard_ids'    => $state->order,
            'index'            => (int) $state->index,
            'created_at'       => $this->convertServerTimeToUTC($state->created_at),
            'updated_at'       => $this->convertServerTimeToUTC($state->updated_at),
        ];
    }
}
