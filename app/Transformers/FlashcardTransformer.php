<?php

namespace App\Transformers;

use App\Flashcard;
use App\Transformers\Transformer;

class FlashcardTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Flashcard $flashcard)
    {
        return [
            'id'           => (int) $flashcard->id,
            'question'     => $flashcard->question,
            'answer'       => $flashcard->answer,
            'imageurl'     => $flashcard->imageurl,
            'source'       => $flashcard->source,
            'step'         => (int) $flashcard->step,
            'category_id'  => (int) $flashcard->category_id,
            'topic_id'     => (int) $flashcard->topic_id,
            'clerkship_id' => (int) $flashcard->clerkship_id,
            'is_active'    => $flashcard->is_active_flashback == 1,
            'created_at'   => $this->convertServerTimeToUTC($flashcard->created_at),
            'updated_at'   => $this->convertServerTimeToUTC($flashcard->updated_at),
        ];
    }
}
