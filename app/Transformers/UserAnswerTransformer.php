<?php

namespace App\Transformers;

use App\Transformers\Transformer;
use App\UserAnswer;

class UserAnswerTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(UserAnswer $answer)
    {
        return [
            'user_id'          => $answer->user_id,
            'flashcard_id'     => (int) $answer->flashcard_id,
            'confidence_level' => (int) $answer->confidence_level,
            'last_flipped_at'  => $this->convertServerTimeToUTC($answer->last_flipped_at),
            'created_at'       => $this->convertServerTimeToUTC($answer->created_at),
            'updated_at'       => $this->convertServerTimeToUTC($answer->updated_at),
        ];
    }
}
