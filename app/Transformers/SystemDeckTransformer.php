<?php

namespace App\Transformers;

use App\SystemDeck;
use App\Transformers\Transformer;
use App\User;

class SystemDeckTransformer extends Transformer
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(SystemDeck $deck)
    {
        return [
            'user_id'             => (int) $deck->user_id,
            'system_deck_type_id' => (int) $deck->flashcard_system_deck_type_id,
            'flashcard_ids'       => $deck->flashcardsByTrack()->pluck('flashcards.id'),
            'created_at'          => $this->convertServerTimeToUTC($deck->created_at),
            'updated_at'          => $this->convertServerTimeToUTC($deck->updated_at),
        ];
    }
}
