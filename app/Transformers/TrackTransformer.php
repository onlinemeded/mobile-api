<?php

namespace App\Transformers;

use App\Track;
use App\Transformers\Transformer;

class TrackTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Track $track)
    {
        return [
            'id'           => (int) $track->id,
            'name'         => $track->name,
            'slug'         => $track->slug,
            'redirect'     => $track->redirect,
            'is_default'   => (int) $track->is_default,
            'created_at'   => $this->convertServerTimeToUTC($track->created_at),
            'updated_at'   => $this->convertServerTimeToUTC($track->updated_at),
        ];
    }
}
