<?php

namespace App\Transformers;

use App\Category;

class CategoryTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Category $category): array
    {
        return [
            'id'           => (int)$category->id,
            'name'         => $category->name,
            'description'  => $category->description,
            'taglines'     => json_decode($category->description_json) ?? [],
            'slug'         => $category->slug,
            'sort'         => (int)$category->sort,
            'clerkship_id' => (int)$category->clerkship_id,
            'is_active'    => $category->is_active_flashback == 1,
            'statistics'  => [
                'topics_count' => $category->topics_count,
                'questions_count' => $category->questions_count,
                'flashcards_count' => $category->flashcards_count,
                'videos_duration' => $category->videos_duration,
            ],
            'created_at'   => $this->convertServerTimeToUTC($category->created_at),
            'updated_at'   => $this->convertServerTimeToUTC($category->updated_at),
        ];
    }
}
