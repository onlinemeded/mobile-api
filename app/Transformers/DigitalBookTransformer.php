<?php

namespace App\Transformers;

use App\User;
use App\DigitalBook;
use App\Transformers\Transformer;

class DigitalBookTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(DigitalBook $digitalBook)
    {
        // reserved to get user products
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        return [
            'id' => $digitalBook->id,
            'title' => $digitalBook->title,
            'description' => $digitalBook->description,
            'src_url' => $digitalBook->src_url,
            'img_url' => $digitalBook->img_url,
            'format_type' => $digitalBook->format_type,
            // need to change this based on billing
            'product_id' => $digitalBook->id,
            'unit_price' => 29.99,
            'can_access' => true,
        ];
    }
}
