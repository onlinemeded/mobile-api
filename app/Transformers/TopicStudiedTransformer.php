<?php

namespace App\Transformers;

use App\TopicStudied;
use App\Transformers\Transformer;

class TopicStudiedTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TopicStudied $topicStudied)
    {
        return [
            'user_id'    => (int) $topicStudied->user_id,
            'topic_id'   => (int) $topicStudied->topic_id,
            'time'       => $this->convertServerTimeToUTC($topicStudied->created_at), //DEPRECATED 4/7/2017
            'studied_at' => $this->convertServerTimeToUTC($topicStudied->created_at),
        ];
    }
}
