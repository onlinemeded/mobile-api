<?php

namespace App\Transformers;

use App\Module;
use App\QBankResult;
use DB;

class ModuleTransformer extends Transformer
{
    protected $includeQuestions = true;
    protected $shouldShuffleQuestionIds = true;

    protected function shuffleQuestionIds($questionids)
    {
        if (!$this->shouldShuffleQuestionIds) {
            return $questionids;
        }

        $qids = explode(',', $questionids);
        shuffle($qids);
        return implode(',', $qids);
    }

    public function noQuestions(): void
    {
        $this->includeQuestions = false;
    }

    public function noQuestionIdsShuffle(): void
    {
        $this->shouldShuffleQuestionIds = false;
    }

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Module $module)
    {
        $numquestions = count(explode(',', $module->questionids));
        $numcorrect = 0;
        $answerAttempts = json_decode($module->answer_attempts, true);
        if ($answerAttempts) {
            foreach ($answerAttempts as $key => $value) {
                $numcorrect += $value['is_correct'] ? 1 : 0;
            }
        }
        $questions = [];
        if ($this->includeQuestions) {
            $questions = $this->getQuestions($module);
        }

        return [
            'id'                => (int) $module->id,
            'user_id'           => (int) $module->user_id,
            'display_name'      => (string) $module->display_name,
            'questionids'       => (string) $this->shuffleQuestionIds($module->questionids),
            'testmode'          => (string) $module->testmode,
            'questionmode'      => (string) $module->questionmode,
            'numquestions'      => $numquestions,
            'numcorrect'        => $numcorrect,
            'complete'          => (int) $module->complete,
            'starttime'         => (int) $module->starttime,
            'timerlength'       => (int) $module->timerlength,
            'finishtime'        => (int) $module->finishtime,
            'active'            => (int) $module->active,
            'created_at'        => $this->convertServerTimeToUTC($module->created_at),
            'updated_at'        => $this->convertServerTimeToUTC($module->updated_at),
            'answer_attempts'   => $answerAttempts,
            'questions'         => $questions,
        ];
    }

    private function getQuestions($module)
    {
        $userId = $module->user_id;
        if (!$userId) {
            $user = app('Dingo\Api\Auth\Auth')->user();
            $userId = $user->id;
        }

        $isCustomModule = isset($module->id);
        if ($isCustomModule) {
            $answerAttempts = json_decode($module->answer_attempts, true);
            $questions = $this->getCustomModuleQuestions($userId, $module->questionids, $answerAttempts);
        } else {
            $questions = $this->getTopicModuleQuestions($userId, $module->questionids);
        }

        if (empty($questions)) {
            return [];
        }

        // match global question performance stats to individual questions
        $questions = $this->getGlobalPerformanceStats($questions);

        return $questions;
    }

    private function getCustomModuleQuestions($userId, $questionIds, $answerAttempts)
    {
        $sql = <<<SQL
SELECT q.*
, (mu.qbank_id IS NOT NULL) AS is_bookmarked
FROM qbanks q
LEFT JOIN markedqbank_user mu ON mu.user_id = ? AND mu.qbank_id = q.id
WHERE q.id IN ($questionIds)
SQL;

        $questions = DB::select($sql, [$userId, $userId]);

        // associate qbank results to questions
        foreach ($questions as $question) {
            if ($answerAttempts && isset($answerAttempts[$question->id])) {
                $attempt = $answerAttempts[$question->id];
                $question->result = [
                    'qbank_id' => $question->id,
                    'result' => $attempt['is_correct'] ? QBankResult::CORRECT : QBankResult::WRONG,
                    'answer' => $attempt['answer'],
                    'is_correct' => $attempt['is_correct'],
                ];
            } else {
                $question->result = null;
            }
        }
        return $questions;
    }

    private function getTopicModuleQuestions($userId, $questionIds)
    {
        if (empty($questionIds)) {
            return [];
        }

        $sql = <<<SQL
SELECT q.*
, (mu.qbank_id IS NOT NULL) AS is_bookmarked
, r.answer
, r.result
FROM qbanks q
LEFT JOIN markedqbank_user mu ON mu.user_id = ? AND mu.qbank_id = q.id
LEFT JOIN results r ON r.user_id = ? AND r.qbank_id = q.id
WHERE q.id IN ($questionIds)
SQL;

        $questions = DB::select($sql, [$userId, $userId]);
        // associate qbank results to questions
        foreach ($questions as $question) {
            $result = [];
            $result['qbank_id'] = $question->id;
            $result['result'] = $question->result;
            $result['answer'] = $question->answer;
            $result['is_correct'] = $question->answer === (int)$question->correctanswer;
            $question->result = $question->answer ? $result : null;
        }
        return $questions;
    }

    private function getGlobalPerformanceStats($questions)
    {
        $questionIds = array_map(function ($q) {
            return $q->id;
        }, $questions);
        $questionIdsCsv = implode(',', $questionIds);

        $sql = <<<SQL
SELECT qbank_id
, answertotal
, answercorrect
, answer1 AS Answer1
, answer2 AS Answer2
, answer3 AS Answer3
, answer4 AS Answer4
, answer5 AS Answer5
, answer6 AS Answer6
, answer7 AS Answer7
, answer8 AS Answer8
FROM qbank_global_stats 
WHERE qbank_id IN ($questionIdsCsv)
SQL;
        $stats = DB::select($sql);
        $stats = array_column($stats, null, 'qbank_id');

        $defaultStats = [
            'answertotal' => 0,
            'answercorrect' => 0,
            'Answer1' => 0,
            'Answer1' => 0,
            'Answer2' => 0,
            'Answer3' => 0,
            'Answer4' => 0,
            'Answer5' => 0,
            'Answer6' => 0,
            'Answer7' => 0,
            'Answer8' => 0
        ];

        foreach ($questions as $question) {
            $question->stats = $stats[$question->id] ?? $defaultStats;
        }

        return $questions;
    }
}
