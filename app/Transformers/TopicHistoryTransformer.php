<?php

namespace App\Transformers;

use App\TopicHistory;
use App\Transformers\Transformer;

class TopicHistoryTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TopicHistory $topicHistory)
    {
        return [
            'user_id'                => (int) $topicHistory->user_id,
            'topic_id'               => (int) $topicHistory->topic_id,
            'time'                   => $this->convertServerTimeToUTC($topicHistory->time), //DEPRECATED 4/7/2017
            'last_viewed_at'         => $this->convertServerTimeToUTC($topicHistory->time),
            'last_synced_at'         => $this->convertServerTimeToUTC($topicHistory->last_synced_at),
            'reviewed_at'            => $this->convertServerTimeToUTC($topicHistory->reviewed_at),
            'next_due_at'            => $this->convertServerTimeToUTC($topicHistory->next_due_at),
            'next_due_interval_days' => (int) $topicHistory->next_due_interval_days,
        ];
    }
}
