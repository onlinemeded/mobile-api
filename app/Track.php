<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Topic;
use App\Flashcard;
use App\Clerkship;
use App\QBankQuestion;

class Track extends Model
{
    protected $table = 'tracks';

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function plans()
    {
        return $this->hasMany('App\Plan');
    }
    
    public function clerkships()
    {
        return $this->hasMany('App\Clerkship');
    }

    public function categories()
    {
        return $this->hasManyThrough('App\Category',
                                     'App\CategoryTrack',
                                     'track_id', // CT FK
                                     'id', // Track PK
                                     'id', // Cat PK
                                     'category_id'); //CT FK
    }

    public function trackProductPermissions()
    {
        return $this->hasMany('App\TrackProductPermissions', 'track_id', 'id');
    }

    public function getClerkships()
    {
        return Cache::remember(
        	__METHOD__ . '::track:' . $this->id,
        	Carbon::MINUTES_PER_HOUR,
        	function() {
                return Clerkship::where('track_id', $this->id)
                    ->orWhereNull('track_id')
                    ->where('is_active_flashback', 1)
                    ->get();
			}
		);
    }

	public function getCategories()
	{
		return Cache::remember(
			__METHOD__ . '::track:' . $this->id,
			Carbon::MINUTES_PER_HOUR,
			function() {
                return $this->categories()
                ->where('is_active_flashback', 1)
                ->get();
			}
		);
    }

    public function getTopics()
    {
        $categoryIds = $this->categories->pluck('id');
        return Topic::whereIn('category_id', $categoryIds)
            ->where('is_active_flashback', 1);
    }

    public function getFlashcards()
    {
        $clerkshipIds = $this->getClerkships()->pluck('id');
        $fixedCategoryIds = array(0);
        $categoryIds = $this->getCategories()->pluck('id');
        return Flashcard::whereIn('clerkship_id', $clerkshipIds)
            ->orWhereIn('category_id', $categoryIds)
            ->orWhereIn('category_id', $fixedCategoryIds)
            ->where('is_active_flashback', 1);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public static function findOrDefault($id) {
        $track = self::find($id);

        if($track) {
            return $track;
        }

        return Track::whereIsDefault(true)->first();
    }
}
