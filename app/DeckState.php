<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeckState extends Model
{
    protected $table = 'flashcard_deck_states';

    protected $fillable = ['user_id', 'entity_type', 'entity_specifier', 'order', 'index', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
