<?php

namespace App\InternBootcamp;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'intern_bootcamp_videos';
    /**
     * The section that belongs to the video.
     */
    public function section()
    {
        return $this->belongsTo('App\InternBootcamp\Section', 'intern_bootcamp_section_id');
    }
}
