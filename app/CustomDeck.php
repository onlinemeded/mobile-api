<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class CustomDeck extends Model
{
    protected $table = 'flashcard_custom_decks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function flashcards()
    {
        return $this->belongsToMany(
            'App\Flashcard',
            'flashcard_custom_deck_flashcards',
            'flashcard_custom_deck_id',
            'flashcard_id'
            )
                ->withTimestamps();
    }

    public function flashcardsByTrack()
    {
        $user    = app('Dingo\Api\Auth\Auth')->user();
        $user    = User::findOrFail($user->id);

        $flashcardIds = $user->careerTrack->getFlashcards()->pluck('id');

        return $this->belongsToMany(
            'App\Flashcard',
            'flashcard_custom_deck_flashcards',
            'flashcard_custom_deck_id',
            'flashcard_id'
        )
            ->whereIn('flashcard_id', $flashcardIds)
            ->withTimestamps();
    }

    public function user()
    {
        $this->belongsTo('App\User');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (!$this->exists && !$this->slug) {
            $this->attributes['slug'] = str_slug($value);
        }
    }
}
