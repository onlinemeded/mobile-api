<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyAnswerTally extends Model
{

    protected $table = 'flashcard_daily_user_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['total', 'user_id', 'flipped_on', 'track_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'flipped_on', 'alert_shown_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getHighAnswerTally()
    {   
        return self::where('user_id', $this->user_id)->where('track_id', $this->user->careerTrack->id)->max('total');
    }
}
