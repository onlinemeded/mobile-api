<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreateProdStripeProducts::class,
        Commands\CreateStageStripeProducts::class,
        Commands\CreateStripeTopicProducts::class,
        Commands\FetchBillingToken::class,
        Commands\FetchOMEPassportToken::class,
        Commands\UpdateStripeTopicPrices::class,
        Commands\NukeTopicsFromStripe::class,
        Commands\SyncTopicsToStripe::class,
        Commands\UpdateStripeTopicPrices::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('ome:fetch-billing-token')
                 ->hourly();
        $schedule->command('ome:fetch-passport-token')
                 ->weekly()
                 ->at('01:00');
    }
}
