<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe;
use Config;
use App\Topic;

class UpdateStripeTopicPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:update-topic-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the price of all skus beginning with mobile_ to 2.99';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Stripe\Stripe::setApiKey(Config::get('services.stripe.secret'));
        $topics = Topic::where('is_active_website', true)->get();
        $allSkus = Stripe\Sku::all();
        foreach ($allSkus->autoPagingIterator() as $sku) {
            if(substr($sku->id, 0,7) == "mobile_") {
                $sku->price = 299;
                $sku->save();
                $this->info("Updated price for " . $sku->product . "...");
            }
        }
            
        
    }
}
