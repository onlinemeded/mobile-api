<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe;
use Config;
use App\Topic;

class CreateStripeTopicProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:create-topic-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates products in stripe for all topics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Stripe\Stripe::setApiKey(Config::get('services.stripe.secret'));
        $topics = Topic::where('is_active_website', true)->get();
        foreach($topics as $topic) {
            $product = Stripe\Product::create([
                "name" => "Lesson: " . $topic->name,
                "type" => "good",
                "description" => "Mobile App Purchase: ". $topic->name,
                "attributes"=> [],
                "shippable" => "true",
                "active" => "true"
            ]);
            $this->info("Created product Lesson: " . $topic->name);
            $sku = Stripe\Sku::create([
                "product" => $product->id,
                "price" => 299,
                "currency" => "usd",
                "inventory" => [
                    "type" => "infinite",
                ],
                "id" => "mobile_lesson_" . $topic->id,
                "active" => "true",
            ]);
            $this->info("Created sku mobile_lesson_" . $topic->id);
        }
    }
}
