<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe;
use Config;
use App\Topic;

class NukeTopicsFromStripe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:nuke-topics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all topic products and skus in stripe';

    private $prodSecret;
    private $stagingSecret;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->stagingSecret = Config::get('stripe.stagingSecret');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Pulling topic product information from stripe");
        $this->setStaging();
        $this->deleteProducts();
    }

    private function setStaging() {
        Stripe\Stripe::setApiKey($this->stagingSecret);
    }

    private function getProducts() {
        $productList = [];
        $fetchProducts = true;
        $options = ["limit" => 100];
        while($fetchProducts) {
            $products = Stripe\Product::all($options);
            $productList = array_merge($productList, $products->data);
            if ($products->has_more) {
                $lastProduct = end($products->data);
                $options["starting_after"] = $lastProduct->id;
            } else {
                $fetchProducts = false;
            }
        }
        return $productList;
    }

    private function deleteProducts() {
        $this->info("Getting Products");
        $products = $this->getProducts();
        //print_r($products);
        foreach($products as $product) {
            $skus = $product->skus;
            echo 'test';
            foreach($product->skus->autoPagingIterator() as $sku) {
                if(substr($sku->id, 0, 13) === "mobile_lesson") {
                    $this->info("Topic found: " . $product->name);
                    $sku->delete();
                    $this->info("Deleted Sku: " . $sku->id);
                    $product->delete();
                    $this->info("Deleted Topic");
                    break;
                }
            }
        }
    }
}
