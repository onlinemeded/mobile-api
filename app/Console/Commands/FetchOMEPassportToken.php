<?php

namespace App\Console\Commands;

use App\ApiKey;
use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class FetchOMEPassportToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ome:fetch-passport-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches Passport API Token from OME Main';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client([
            'base_uri' => config('main.url')
        ]);

        $response = $client->post('oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => config('passport.client.id'),
                'client_secret' => config('passport.client.secret'),
                'scope' => '*'
            ]
        ]);

        $body = json_decode($response->getBody(), true);

        $apiKey = ApiKey::updateOrCreate(
            [
                'property_id' => 'flashback-api',
                'token_type' => $body['token_type'],
                'refresh_token' => '',
            ],
            [
                'access_token' => $body['access_token'],
                'expires_in' => $body['expires_in'],
            ]
        );

        Cache::forget('flashback-api-passport-token');
        Cache::remember(
            'flashback-api-passport-token',
            Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY * Carbon::DAYS_PER_WEEK,
            function () use ($apiKey) {
                return $apiKey->access_token;
            }
        );
        collect($body)->each(function ($value, $key) {
            $this->line("{$key}: {$value}");
        });
    }
}
