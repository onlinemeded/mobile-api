<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Exception\StoreResourceFailedException;
use App\Http\Resources\Collection;
use App\Transformers\UserAnswerTransformer;
use App\PaceMetric;
use App\UserMetric;
use App\UserAnswer;
use App\User;
use Validator;

class UserAnswerController extends Controller
{
    /**
     * Responds to requests to GET /user-answers
     */
    public function index(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $flashcardIds = $this->getUserCareerTrack()->getFlashcards()->pluck('id');            
        $answers = $user
                    ->answers()
                    ->whereIn('flashcard_id', $flashcardIds)
                    ->get();

        return new Collection($answers, new UserAnswerTransformer);
    }

    /**
     * Responds to requests to POST /user-answers
     */
    public function store(Request $request)
    {
        $payload = $request->only('flashcard_id', 'confidence_level', 'last_flipped_at', 'updated_at');

        $validator = Validator::make($payload, [
            'flashcard_id'     => 'required|exists:flashcards,id',
            'confidence_level' => 'integer',
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new user answer.', $validator->errors());
        }

        $user = app('Dingo\Api\Auth\Auth')->user();

        $answer = $user->answers()
            ->where('flashcard_id', $payload['flashcard_id'])
            ->first();

        if ($answer) {
            if (isset($payload['confidence_level'])) {
                $answer->confidence_level = $payload['confidence_level'];
            }
        } else {
            if (empty($payload['confidence_level'])) {
                $payload['confidence_level'] = 0;
            }

            $answer = UserAnswer::create([
                'flashcard_id'     => $payload['flashcard_id'], // already validated the id above
                'confidence_level' => $payload['confidence_level'],
                'user_id'          => $user->id,
            ]);
        }

        $updated_at = Carbon::now();
        if (isset($payload['last_flipped_at']) || isset($payload['updated_at'])) {
            if (isset($payload['last_flipped_at'])) {
                $last_flipped_at         = $this->convertUtcToServerLocal($payload['last_flipped_at']);
                $answer->last_flipped_at = $last_flipped_at->toDateTimeString();
            }

            if (isset($payload['updated_at'])) {
                $updated_at         = $this->convertUtcToServerLocal($payload['updated_at']);
                $answer->updated_at = $updated_at->toDateTimeString();
            }

            $answer->save();
        }

        if (empty($payload['confidence_level'])) {
            $payload['confidence_level'] = 0;
        }
        UserMetric::create([
            'user_id'        => $user->id,
            'type'           => 'flashcard',
            'object_id'      => $payload['flashcard_id'],
            'is_remediation' => false,
            'value'          => $payload['confidence_level'],
            'description'    => 'User completed a flashcard',
            'updated_at'     => $updated_at,
            'ip'             => $request->ip(),
        ]);

        // Note the intentional use of 'App\Flashcard' instead of Flashcard::class here,
        // to communicate that the value is defined by ome-main's model definition.
        PaceMetric::createPaceMetric($user->id, PaceMetric::$readAction, 'App\Flashcard', $payload['flashcard_id']);

        return $this->response->item($answer, new UserAnswerTransformer);
    }
}
