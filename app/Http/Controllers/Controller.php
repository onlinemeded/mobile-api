<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Config;
use App\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Helpers;
    
function endsWith($haystack, $needle)
{
    return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

    protected function convertUtcToServerLocal($dateString)
    {
        if ($this->endsWith($dateString, 'Z')) {
            // handles ZULU timezone format coming out of mobile
            return Carbon::createFromFormat('Y-m-d\TH:i:s.u\Z', $dateString, 'UTC')->setTimezone(Config::get('app.timezone'));
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $dateString, 'UTC')->setTimezone(Config::get('app.timezone'));
    }

    protected function getUser(): User {
        return app('Dingo\Api\Auth\Auth')->user();
    }

    protected function getUserId() {
        return $this->getUser()->id; 
    }

    protected function getUserCareerTrack() {
        return $this->getUser()->careerTrack; 
    }

    protected function getUtcDate()
    {
        return Carbon::now()->setTimezone('UTC');
    }

    protected function getUserLocalizedToday($UTCOffsetMinutes)
    {
        return $this->getUtcDate()->addMinutes($UTCOffsetMinutes);
    }

    protected function getUserLocalizedTodayFormatted($UTCOffsetMinutes)
    {
        return $this->getUserLocalizedToday($UTCOffsetMinutes)->toDateString();
    }
}
