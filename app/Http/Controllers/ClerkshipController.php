<?php

namespace App\Http\Controllers;

use App\Clerkship;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ClerkshipController extends Controller
{
	/**
	 * Responds to requests to GET /clerkships
	 */
	public function index(Request $request)
	{
		$clerkships = \Cache::remember(
			__METHOD__.'::track:'.$this->getUserCareerTrack()->id,
			Carbon::MINUTES_PER_HOUR,
			function () {
				return [
					'data' => $this->getUserCareerTrack()->getClerkships()
						->map(function (Clerkship $clerkship) {
							return [
								'id'          => (int)$clerkship->getAttributeValue('id'),
								'name'        => $clerkship->getAttributeValue('name'),
								'description' => $clerkship->getAttributeValue('description'),
								'slug'        => $clerkship->getAttributeValue('slug'),
								'sort'        => (int)$clerkship->getAttributeValue('sort'),
								'tier'        => (int)$clerkship->getAttributeValue('tier'),
								'is_active'   => $clerkship->is_active_flashback == 1,
							];
						}),
				];
			}
		);

		return response()->json($clerkships);
	}

}
