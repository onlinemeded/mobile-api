<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Validator;
use Customer;
use Cart;
use Product;
use Dingo\Api\Auth\Auth;

use ReceiptValidator\iTunes\Validator as iTunesValidator;
use ReceiptValidator\GooglePlay\Validator as GooglePlayValidator;

class MicroTransactionController extends Controller
{
    public const ANDROID_PURCHASE_VALIDATOR_NAME = 'Backend Validation';
    public const ANDROID_APP_PACKAGE_NAME = 'com.onlinemeded.mobile';
    private const ANDROID_SERVICE_ACCOUNT_KEY_FILE = 'google-service-account-for-iap-validation.json';

    public function verifySku(string $sku)
    {
        $productResponse = Product::get(['sku' => $sku]);
        if ($productResponse['ok']) {
            if (count($productResponse['response']) === 1) {
                return response()->json($productResponse['response'][0]);
            } else {
                return response()->json([
                   'error' => 'Product not found',
                ], Response::HTTP_NOT_FOUND);
            }
        } else {
            return response()->json([
                'error' => 'Could not connect to billing',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postInAppPurchase(Request $request)
    {
        $payload = $request->only('sku', 'platform', 'receipt');
        $validator = Validator::make($payload, [
            'sku'        => 'required',
            'platform'   => 'required', // [ios|android]
            'receipt'    => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        $productResponse = Product::get(['sku' => $payload['sku']]);
        if (!$productResponse['ok']) {
            return response()->json([
                'error' => 'Product not found',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } elseif (count($productResponse['response']) !== 1) {
            return response()->json([
                'error' => 'Product not found',
            ], Response::HTTP_NOT_FOUND);
        }

        $product = $productResponse['response'][0];

        // validate store receipt
        $storeTransactionId = null;
        if ($payload['platform'] === 'ios') {
            $storeTransactionId = self::validateIosReceipt($payload['sku'], $payload['receipt']);
        } else {
            $storeTransactionId = self::validateAndroidReceipt($payload['sku'], $payload['receipt']);
        }
        if (!$storeTransactionId) {
            return response()->json([
                'error' => 'Could not validate purchase receipt',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var User $user */
        $user = app(Auth::class)->user();
        $customer = Customer::getOrCreate($user);
        $checkout = Cart::checkoutThirdParty(
            $customer,
            [$product->id],
            $payload['platform'],
            // Invoice Number
            $storeTransactionId,
            date('Y-m-d')
        );

        // reset product access cache
        \Cache::forget(ProductsController::productAccessPurchasesCacheKey($user));

        return response()->json([
            'data' => [
                'success' => true,
                'receiptId' => $storeTransactionId,
                'checkout' => $checkout,
            ]
        ]);
    }

    private static function validateIosReceipt($sku, $receiptBase64Data)
    {
        $endpoint = \App::environment('production') ? iTunesValidator::ENDPOINT_PRODUCTION : iTunesValidator::ENDPOINT_SANDBOX;
        $validator = new iTunesValidator($endpoint);
        $response = $validator->setReceiptData($receiptBase64Data)->validate();
        $transactionId = null;

        if ($response->isValid()) {
            foreach ($response->getPurchases() as $purchase) {
                $productId = $purchase->getProductId();
                if ($productId === $sku) {
                    $transactionId = $purchase->getTransactionId();
                }
            }
            return $transactionId;
        } else {
            \Bugsnag::notifyError(
                'Invalid iOS Receipt',
                "Invalid iOS AppStore Receipt. Receipt result code = $response->getResultCode()"
            );
            return null;
        }
    }

    private static function validateAndroidReceipt($productId, $purchaseToken)
    {
        $googleClient = new \Google_Client();
        $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setApplicationName(self::ANDROID_PURCHASE_VALIDATOR_NAME);
        $googleClient->setAuthConfig(self::googleServiceAccountFilePath());

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($googleClient);
        $validator = new GooglePlayValidator($googleAndroidPublisher);

        try {
            $response = $validator->setPackageName(self::ANDROID_APP_PACKAGE_NAME)
                ->setProductId($productId)
                ->setPurchaseToken($purchaseToken)
                ->validatePurchase();
            return $response->getRawResponse()->orderId;
        } catch (\Exception $e) {
            \Bugsnag::notifyException($e);
            return null;
        }
    }

    public static function googleServiceAccountFilePath(): string
    {
        return storage_path(self::ANDROID_SERVICE_ACCOUNT_KEY_FILE);
    }
}
