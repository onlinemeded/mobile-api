<?php

namespace App\Http\Controllers;

use App\ApiKey;
use App\Social;
use App\Transformers\UserTransformer;
use App\User;
use App\Track;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class UserController extends Controller
{
    /**
     * Creates new social link for users
     */
    public function linkSocial(Request $request)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->only('uid', 'provider', 'oauth2_token', 'profile_photo_url'), [
            'uid' => 'required',
            'provider' => 'required',
            'oauth2_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], 400);
        }

        $params             = $request->only('uid', 'provider', 'oauth2_token', 'profile_photo_url');
        $params['provider'] = strtolower($params['provider']);
        $supportedProviders = ['facebook', 'google'];

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        // We've already validated the user, so let's change the
        // profile pic even if the social link below fails
        if (!empty($params['profile_photo_url']) && $user->imageurl == null) {
            $user->imageurl = $params['profile_photo_url'];
            $user->save();
        }

        // check provider
        if (!in_array($params['provider'], $supportedProviders)) {
            return response()->json([
                'error' => 'Provider not supported',
            ], 400);
        }
        
        // check if the there is another account associated
        $social = Social::where('uid', $params['uid'])
            ->where('provider', $params['provider'])
            ->first();

        if ($social) {
            return response()->json([
                'error' => 'There is an account already linked with that ' . ucwords($params['provider']) . ' user',
            ], 401);
        }

        // check if user has already linked the account
        $social = Social::where('user_id', $user->id)
            ->where('provider', $params['provider'])
            ->first();

        if ($social) {
            return response()->json([
                'error' => 'User is already linked with ' . ucwords($params['provider']),
            ], 400);            
        }
        
        // create social link
        $social = Social::create([
            'user_id'             => $user->id,
            'uid'                 => $params['uid'],
            'provider'            => $params['provider'],
            'oauth2_access_token' => $params['oauth2_token'],
            'oauth2_expires'      => Carbon::now()->addMonths(2),
        ]);

        if ($social) {
            return response()->json([
                'success' => 'New link created with ' . ucwords($params['provider']),
            ], 200);
        } else {
            return response()->json([
                'error' => 'There was an error creating a new social link. Please try again later.',
            ], 500);
        }
    }

    /**
     * Removes social link for users
     */
    public function unLinkSocial(Request $request)
    {
        $validator = Validator::make($request->only('provider'), [
            'provider' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], 400);
        }
        
        $params             = $request->only('provider');
        $params['provider'] = strtolower($params['provider']);
        $supportedProviders = ['facebook', 'google'];

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        if (!in_array($params['provider'], $supportedProviders)) {
            return response()->json([
                'error' => 'Provider not supported',
            ], 400);
        }
        

        $rows = Social::where('user_id', $user->id)
            ->where('provider', $params['provider'])
            ->delete();

        if ($rows == 0) {
            return response()->json([
                'error' => 'User is not linked with ' . ucwords($params['provider']),
            ], 400);
        } else {
            return response()->json([
                'success' => 'Link with ' . ucwords($params['provider']) . ' removed',
            ], 200);
        }
    }

    /**
     * Set Career Track
     */
    public function setCareerTrack(Request $request)
    {
        $params = $request->only('career_track_id');
        if (!empty($params['career_track_id'])) {

            $count = Track::where('id','=',$params['career_track_id'])->count();
            if ($count < 1) {
                return response()->json([
                    'error' => 'Invalid career track',
                ]);
            }


            $user           = app('Dingo\Api\Auth\Auth')->user();
            $user           = User::findOrFail($user->id);
            $user->career_track_id = $params['career_track_id'];
            $user->save();
            return $this->response->item($user, new UserTransformer);
        } else {
            return response()->json([
                'error' => 'Please provide a career track id',
            ]);
        }
    }

    public function registerMobileLogin(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);
        
        $isFirstLogin = false;
        $loginTimestamp = time();

        if ($user->mobile_first_login == null) {
            $user->mobile_first_login = $loginTimestamp;
            $isFirstLogin = true;
        }

        $user->mobile_last_login = $loginTimestamp;
        $user->save();

        return response()->json([
            'is_first_login' => $isFirstLogin,
            'mobile_first_login' => time($user->mobile_first_login),
            'mobile_first_login_datetime' => $user->mobile_first_login->setTimezone('UTC')->toDateTimeString(),
            'mobile_last_login' => time($user->mobile_last_login),
            'mobile_last_login_datetime' => $user->mobile_last_login->setTimezone('UTC')->toDateTimeString()
        ], Response::HTTP_CREATED);
    }

    public function getDownloadLimit()
    {
        $user = $this->getUser();
        $client = new Client([
            'base_uri' => config('main.url')
        ]);
        $accessToken = ApiKey::byProperty('flashback-api');
        $response = $client->get("api/v1/user/$user->id/download-limit", [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
        ]);

        return json_decode($response->getBody(), true);
    }
}
