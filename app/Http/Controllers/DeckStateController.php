<?php

namespace App\Http\Controllers;

use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Collection;
use App\Transformers\DeckStateTransformer;
use App\Category;
use App\Clerkship;
use App\DeckState;
use App\SystemDeckType;
use App\Topic;
use App\User;
use Config;
use Validator;

class DeckStateController extends Controller
{
    /**
     * Responds to requests to GET /deck-states
     */
    public function getStates(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $params = $request->only('updated_since', 'entity_type', 'entity_specifier');

        if (isset($params['entity_type']) && isset($params['entity_specifier']))
        {
            $deck = $user->deckStates()->where('entity_type', $params['entity_type'])->where('entity_specifier', $params['entity_specifier'])->firstOrFail();
            return $this->response->item($deck, new DeckStateTransformer);
        }
        else
        {
            if (!isset($params['updated_since']))
            {
                $params['updated_since'] = 0;
            }

            $updatedSinceTimestamp = strtotime($params['updated_since']);

            if ($updatedSinceTimestamp)
            {
                $updatedSinceTimestamp = strtotime($params['updated_since'] . 'UTC'); // this time specify UTC
                $deckStates = $user->deckStates()
                    ->whereDate('updated_at', '>', date('Y-m-d H:i:s', $updatedSinceTimestamp))
                    ->when(isset($params['entity_type']), function ($query) use ($params) {
                        return $query->where('entity_type', $params['entity_type']);
                    })
                    ->orderBy('id')
                    ->get();
            }
            else
            {
                $deckStates = $user->deckStates()
                    ->orderBy('id')
                    ->when(isset($params['entity_type']), function ($query) use ($params) {
                        return $query->where('entity_type', $params['entity_type']);
                    })
                    ->get();
            }

            return new Collection($deckStates, new DeckStateTransformer);
        }
    }

    /**
     * Responds to requests to POST /deck-states
     */
    public function setState(Request $request)
    {
        $user    = app('Dingo\Api\Auth\Auth')->user();
        $user    = User::findOrFail($user->id);
        $payload = $request->only('entity_type', 'entity_specifier', 'flashcard_ids', 'index', 'created_at', 'updated_at');

        $validator = Validator::make($payload, [
            'entity_specifier' => 'required|integer',
            'entity_type'      => 'required|in:topic,category,clerkship,user_deck,system_deck',
        ]);

        if (empty($payload['flashcard_ids'])) {
            $payload['flashcard_ids'] = '';
        }

        if (empty($payload['index'])) {
            $payload['index'] = -1;
        }

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not save deck state.', $validator->errors());
        } else {

            $object = null;

            switch ($payload['entity_type']) {
                case 'topic':
                    $object = Topic::findOrFail($payload['entity_specifier']);
                    break;
                case 'category':
                    $object = Category::findOrFail($payload['entity_specifier']);
                    break;
                case 'clerkship':
                    $object = Clerkship::findOrFail($payload['entity_specifier']);
                    break;
                case 'user_deck':
                    $object = $user->customDecks()->findOrFail($payload['entity_specifier']);
                    break;
                case 'system_deck':
                    $object = SystemDeckType::findOrFail($payload['entity_specifier']);
                    break;
            }

            if ($object) {
                $state = DeckState::where('entity_type', $payload['entity_type'])
                    ->where('entity_specifier', $object->id)
                    ->where('user_id', $user->id)
                    ->first();

                if ($state) {
                    $state->index = $payload['index'];
                    $state->order = $payload['flashcard_ids'];

                    $state->save();
                } else {
                    $state = DeckState::create([
                        'entity_type'      => $payload['entity_type'],
                        'entity_specifier' => $object->id,
                        'user_id'          => $user->id,
                        'index'            => $payload['index'],
                        'order'            => $payload['flashcard_ids'],
                    ]);
                }

                // allow overriding the created at or updated at field
                if (isset($payload['created_at']) || isset($payload['updated_at'])) {
                    if (isset($payload['created_at'])) {
                        $created_at        = $this->convertUtcToServerLocal($payload['created_at']);
                        $state->created_at = $created_at->toDateTimeString();
                    }

                    if (isset($payload['updated_at'])) {
                        $updated_at        = $this->convertUtcToServerLocal($payload['updated_at']);
                        $state->updated_at = $updated_at->toDateTimeString();
                    }

                    $state->save();
                }

                $response = [
                    'message' => 'The deck state was stored successfully',
                ];

                return $response;
            } else {
                abort(404);
            }
        }
    }

}
