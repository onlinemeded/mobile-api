<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;


class CountryController extends Controller
{
    public function listCountries(Request $request)
    {
      $select = $request->input('select');  
      $code = $request->input('code');
      $name = $request->input('name');
      
      // default selected columns
      if ($select == null) {
        $select = 'code,name';
      }

      $selectArr = explode(',',$select);

      $where = [];

      if ($code) {
        array_push($where, ['code', '=', $code]);
      }

      if ($name) {
        array_push($where, ['name', 'like', $name]);
      }

      $payload = new \stdClass;
      
      $payload->data = Country::where($where)->select($selectArr)->get()->sortBy(function ($country, $key) {
        if ($country->code === "US") return '';
        return $country->code;
      })->values();

      return response()->json($payload);
    }
};
