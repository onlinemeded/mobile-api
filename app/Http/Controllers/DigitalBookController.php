<?php
// (DEPRECATED) @TODO: Remove with dependencies (digital-books react application)
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DigitalBook;
use App\Transformers\DigitalBookTransformer;
use Validator;

class DigitalBookController extends Controller // (DEPRECATED)
{
    public function __construct()
    {

        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware('jwt.auth', ['except' => [
                'reader',
            ],
        ]);
    }

    /**
     * Serves a book through a small React application.
     *
     * @return View
     */
    public function reader($bookId = null)
    {        
        $digitalBook = DigitalBook::where('id', $bookId)->first();

        if ($digitalBook == null) {
            return view('epubReader', [
                'title' => null,
                'url' => null,
            ]);
        }

        return view('epubReader', [
            'title' => $digitalBook->title,
            'url' => $digitalBook->src_url,
        ]);
    }

    public function getUserDigitalBooks($format_type = null, Request $request)
    {
        if ($format_type == null) {
            $digitalBooks = DigitalBook::where('active', 1)->get();
        } else {
            $match = ['active' => 1, 'format_type' => $format_type];
            $digitalBooks = DigitalBook::where($match)->get();
        }
        
        $data = [];
        $transformer = new DigitalBookTransformer();
        foreach ($digitalBooks as $key=>$digitalBook) {
            $data[$key] = $transformer->transform($digitalBook);
        }

        $payload = [ 'data' => $data ];

        return response()->json($payload);
    }
}
