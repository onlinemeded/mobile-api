<?php

namespace App\Http\Controllers\InternBootcamp;

use App\Http\Controllers\Controller;
use App\InternBootcamp\Section;

class SectionController extends Controller
{
    public function index() {
        return response()->json([
            'data' =>  Section::where('is_active', 1)->get()
        ]);
    }

    public function show($section) {
        return response()->json([
            'data' => Section::find($section),
        ]);
    }
}
