<?php

namespace App\Http\Controllers;

use App\ApiKey;
use App\Topic;
use App\Transformers\TopicTransformer;
use App\Http\Resources\Collection;
use App\User;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use GuzzleHttp\Client;

class TopicController extends Controller
{
    /**
     * Responds to requests to GET /topics
     */
    public function index(Request $request)
    {
        $topics = $this->getUserCareerTrack()
                             ->getTopics()
                             ->orderBy('sort')
                             ->get();

        return new Collection($topics, new TopicTransformer);
    }

    private function getVideoSource($videoId, $size)
    {
        $json  = $this->fetchJWPlayerPayload($videoId);

        foreach ($json['playlist'][0]['sources'] as $source) {
            if (isset($source['height']) && $source['height'] == $size) {
                return $source['file'];
            }
        }
    }

    private function getVideoCloseCaption($videoId)
    {
        $json  = $this->fetchJWPlayerPayload($videoId);

        $captionUrl = null;

        if (isset($json['playlist']) && count($json['playlist']) >= 1) {
            if (isset($json['playlist'][0]['tracks']) && count($json['playlist'][0]['tracks'])) {
                foreach ($json['playlist'][0]['tracks'] as $key => $value) {
                    if ($value['kind'] == 'captions') {
                        $captionUrl = $value['file'];
                    }
                }
            };
        }

        return $captionUrl;
    }

    private function getAudioSource($videoId)
    {
        $json  = $this->fetchJWPlayerPayload($videoId);
        foreach ($json['playlist'][0]['sources'] as $source) {
            if ($source['type'] === 'audio/mp4') {
                return $source['file'];
            }
        }
    }

    private function fetchJWPlayerPayload($videoId)
    {
        return Cache::remember(
            __METHOD__ . '::video_id:' . $videoId,
            Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY,
            function () use ($videoId) {
                $jwplayerUrl = 'https://cdn.jwplayer.com/v2/media/' . $videoId;
                return json_decode(file_get_contents($jwplayerUrl), true);
            }
        );
    }

    private function getUrlFromTopic($contentType, Topic $topic)
    {
        switch ($contentType) {
            case 'video':
                return $this->getVideoSource($topic->free_video_id, 720);
            case 'video_cc':
                return $this->getVideoCloseCaption($topic->free_video_id);
            case 'audio':
                return $this->getAudioSource($topic->free_video_id);
            case 'notes':
                return $topic->getNoteUrlAttribute();
            case 'whiteboard':
                return $topic->getWhiteboardUrlAttribute();
            default:
                return null;
        }
    }

    private function getPathForTracking($contentType, $topic)
    {
        $sansExtension = str_replace([':', '/'], '', $topic->category->name . ' - ' . $topic->name);

        switch ($contentType) {
            case 'video':
                return "/file/video/$topic->id." . $this->getFileExt($contentType);
            case 'video_cc':
                return "/file/video_cc/$topic->id." . $this->getFileExt($contentType);
            case 'audio':
                return "/file/audio/$topic->id/$sansExtension." . $this->getFileExt($contentType);
            case 'notes':
                return "/file/notes/$topic->id/$sansExtension." . $this->getFileExt($contentType);
            case 'whiteboard':
                return "/file/whiteboard/$topic->id/$sansExtension." . $this->getFileExt($contentType);
        }
    }

    private function getFileExt($contentType)
    {
        switch ($contentType) {
            case 'video':
                return 'mp4';
            case 'video_cc':
                return 'srt';
            case 'audio':
                return 'mp3';
            case 'notes':
                return 'pdf';
            case 'whiteboard':
                return 'pdf';
        }
    }

    public function getResponseContentType($contentType)
    {
        switch ($contentType) {
            case 'video':
                return 'video/mp4';
            case 'video_cc':
                return 'text/srt';
            case 'audio':
                return 'audio/mpeg';
            case 'notes':
                return 'application/pdf';
            case 'whiteboard':
                return 'application/pdf';
        }
    }

    public function getAssetUrl($contentType, $topicId)
    {
        /** @var User $user */
        $user = $this->getUser();
        $topic = Topic::findOrFail($topicId);
        $url = $this->getUrlFromTopic($contentType, $topic);

        if ($url === null) {
            return response()->json([
                'message' => 'Unsupported Content-Type',
                'error' => null,
            ], Response::HTTP_BAD_REQUEST);
        }

        $client = new Client([
            'base_uri' => config('main.url')
        ]);

        $accessToken = ApiKey::byProperty('flashback-api');
        $response = $client->get("api/v1/user/$user->id/download-limit/topic/$topicId/type/$contentType", [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
        ]);

        $downloadLimits = json_decode($response->getBody());

        return response()->json([
            'url' => $url,
            'download_limits' => $downloadLimits,
            'has_already_downloaded' => $user->hasAlreadyDownloadedTopic($topicId, $contentType)
        ]);
    }

    public function confirmDownload($contentType, $topicId)
    {
        /** @var User $user */
        $user = $this->getUser();
        $client = new Client([
            'base_uri' => config('main.url')
        ]);
        $accessToken = ApiKey::byProperty('flashback-api');
        $payload = [
            'type' => $contentType,
            'topic_id' => $topicId,
            'user_id' => $user->id,
        ];
        $response = $client->post('api/v1/user/' . $user->id . '/download-limit', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
            'form_params' => $payload,
        ]);
        return $response->getBody()->getContents();
    }
}
