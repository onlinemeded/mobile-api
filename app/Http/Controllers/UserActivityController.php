<?php

namespace App\Http\Controllers;

use App\User;
use App\UserMetric;
use App\Topic;
use DB;
use App\QBankResult;
use App\QBankBookmark;

class UserActivityController extends Controller
{

    public function getRecentlyWatchedVideos()
    {
        $topicIds = $this->getUserCareerTrack()->getTopics()->pluck('id');
        $metrics = UserMetric::where('user_id', $this->getUserId())
            ->where('type', 'video')
            ->whereIn('object_id', $topicIds)
            ->orderByDesc('updated_at')
            ->take(5)
            ->get();

        foreach ($metrics as $metric) {
            $topic = Topic::find($metric->object_id);
            $metric->topic = $topic;
        }

        return response()->json([
            'data' => $metrics
        ]);
    }

    public function getQbankSummary()
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $questionsView = DB::select("
            SELECT
                qbanks.id,
                qbanks.category_id,
                IF (results.result IS NULL, NULL, IF (results.result = 'C', TRUE, FALSE)) AS is_correct,
                IF (bookmarked.qbank_id, TRUE, FALSE) as is_bookmarked
            FROM qbanks
                JOIN category_track ON category_track.category_id = qbanks.category_id AND category_track.track_id = :career_track_id
                LEFT JOIN results ON results.qbank_id = qbanks.id AND user_id = :user_id_1
                LEFT JOIN markedqbank_user AS bookmarked ON bookmarked.qbank_id = qbanks.id AND bookmarked.user_id = :user_id_2
            WHERE status = 'Approved'",
            [
                'career_track_id' => $user->career_track_id,
                'user_id_1' => $user->id,
                'user_id_2' => $user->id,
            ]
        );

        $questionsIds = array_column($questionsView, 'id');    
        $totalCount = count($questionsIds);

        $correctCount = QBankResult::whereIn('qbank_id', $questionsIds)
            ->where('user_id', $user->id)
            ->where('result', QBankResult::CORRECT)
            ->count();
        
        $incorrectCount = QBankResult::whereIn('qbank_id', $questionsIds)
            ->where('user_id', $user->id)
            ->where('result', QBankResult::WRONG)
            ->count();
        
        $bookmarked = QBankBookmark::whereIn('qbank_id', $questionsIds)
            ->where('user_id', $user->id)
            ->count();

        $payload = [
            'summary' => [
                'qbank' => [
                    'total' => $totalCount,
                    'correct' => $correctCount,
                    'wrong' => $incorrectCount,
                    'bookmarked' => $bookmarked,
                    'questions' => $questionsView
                ]
            ]
        ];

        return response()->json($payload);
    }
};