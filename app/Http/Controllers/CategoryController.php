<?php

namespace App\Http\Controllers;

use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;
use App\User;
use DB;

class CategoryController extends Controller
{
    /**
    * Responds to requests to GET /categoriesWithStats
    */
    public function withStats()
    {
        $user    = app('Dingo\Api\Auth\Auth')->user();
        $user    = User::findOrFail($user->id);
    
    /*
    ** We need to filter the OMM category from this request in case the user is not part of a school that teaches
    ** Osteopathic Medicine or is not his/her profession.
    */
        $sql = <<<SQL
SELECT EXISTS (
        SELECT * FROM schools 
        WHERE schools.id = ? 
        AND (schools.degree = 'DO' OR UPPER(schools.name) LIKE '%OSTEOPATHIC%')
    UNION
        SELECT * FROM profession_user
        JOIN professions on profession_user.profession_id = professions.id
        WHERE user_id = ? && UPPER(professions.name) LIKE '%OSTEOPATHIC%'
) as should_have_omm_category;
SQL;

        $shouldHaveOMMCategory = DB::selectOne($sql, [$user->school_text_id, $user->id])->should_have_omm_category;
                
        $categories = $this->getUserCareerTrack()
                           ->categories()
                           ->where('is_active_flashback', 1)
                           ->orderBy('sort')
                           ->statsData()
                           ->get();

        // @TODO: refactor the code below to not have "manually built response for this asset"
        $data = [];
        $transformer = new CategoryTransformer();
        
        foreach ($categories as $category) {
            // skip OMM category if it should not be included
            if (!$shouldHaveOMMCategory && $category->name == 'OMM') {
                continue;
            }
            
            // attach required stats
            $videos_duration = 0;
            foreach ($category->topics as $topic) {
                $videos_duration += $topic->duration;
            }

            $category->videos_duration = ((int)($videos_duration / 60));
            $data[] = $transformer->transform($category);
        }

        return response()->json([ "data" => $data ]);
    }
}
