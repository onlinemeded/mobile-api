<?php

namespace App\Http\Controllers;

use App\Institution;
use Illuminate\Http\Request;


class InstitutionController extends Controller
{
    public function listInstitutions(Request $request)
    {
       
    $country = $request->input('country');
    
    $institutions = [];
    // Perform search
    if ($country != null) {
        $institutions = Institution::where([
            ['slug', '!=', null],
            ['country', 'like', '%'.$country.'%']
        ])
        ->orWhere('slug', 'other');
    } else {
        $institutions = Institution::where('slug', '!=', null);
    }

    $payload = [
        'data' => $institutions
                    ->select(['id', 'slug', 'name'])
                    ->get()
                    ->sortBy(function ($institution, $key) {
                        if ($institution->slug == 'other') {
                            return 'zzzz';
                        }

                        return $institution->slug;
                    })
                    ->values()
    ];
      
      return response()->json($payload);
    }
};