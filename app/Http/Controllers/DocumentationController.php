<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Topic;
use App\Transformers\TopicTransformer;
use Config;
use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    /**
     * Master Swagger Documentation
     *
     * @SWG\Swagger(
     *     basePath="/api/v1",
     *     schemes={"http", "https"},
     *     host=L5_SWAGGER_CONST_HOST,
     *     @SWG\Info(
     *         version="330.0.1",
     *         title="OnlineMedEd API",
     *         description="Restful Laravel API for OnlineMedEd Projects"
     *     )
     * )
     *
     * @SWG\Get(
     *     path="/documentation",
     *     tags={"Documentation"},
     *     @SWG\Response(response=200, description="API swagger documentation")
     * )
     *
     */
    public function swagger()
    {
      header('Content-type: text/yaml');
      header("Access-Control-Allow-Origin: *");
      die(file_get_contents('../swagger.yaml'));
    }
}