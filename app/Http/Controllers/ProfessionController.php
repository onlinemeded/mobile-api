<?php

namespace App\Http\Controllers;

use App\Profession;
use App\Http\Controllers\Controller;
use Config;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;


class ProfessionController extends Controller
{
    public function listProfessions(Request $request)
    {
      $professions = Profession::orderBy('sort','desc')->get();
      $payload = new \stdClass;
      $payload->data = $professions;
      return response()->json($payload);
    }
};