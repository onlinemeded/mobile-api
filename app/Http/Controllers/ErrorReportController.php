<?php

namespace App\Http\Controllers;

use App\ErrorReport;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Validator;

class ErrorReportController extends Controller
{
    public function store(Request $request)
    {
        $payload = $request->only('type', 'id', 'reason', 'sub_reason');

        $validator = Validator::make($payload, [
            'id'     => 'required|integer',
            'type'   => 'required',
            'reason' => 'required',
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new report.', $validator->errors());
        } else {
            $user = app('Dingo\Api\Auth\Auth')->user();
            ErrorReport::create([
                'user_id'       => $user->id,
                'object_id'     => $payload['id'],
                'type'          => $payload['type'],
                'reason'        => $payload['reason'],
                'sub_reason'    => $payload['sub_reason'],
            ]);

            $response = [
                'message' => 'The report was created successfully',
            ];

            return $response;
        }
    }
}
