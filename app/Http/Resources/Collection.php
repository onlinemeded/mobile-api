<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Collection extends ResourceCollection
{
    /**
     * Create a new resource collection.
     *
     * @param  mixed  $collection
     * @param \App\Transformers\Transformer transformer
     * @return void
     */
    public function __construct($collection, $transformer = null)
    {
        parent::__construct($collection);
        $this->transformer = $transformer;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->transformer != null)
        {
            $data = [];
            foreach ($this->collection as $key=>$item)
            {
                $data[$key] = $this->transformer->transform($item);
            }

            return ['data' => $data];
        }

        return [ 'data' => $this->collection ];
    }
}
