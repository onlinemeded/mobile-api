<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class EnforceCareerTrack
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user->career_track_id = (int) $request->header('x-ome-user-career-track-id', $user->career_track_id);
        $user->save();

        return $next($request);
    }
}
