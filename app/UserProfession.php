<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfession extends Model
{
    protected $table = 'profession_user';

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    protected $fillable = [
        'user_id',
        'profession_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
