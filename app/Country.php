<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $table = 'countries';
  protected $fillable = ['code', 'name'];

  public function institution()
  {
      return $this->hasMany('App\Institution', 'country', 'name');
  }

}
