<?php

namespace App\Billing;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

/**
 * @property \Illuminate\Foundation\AliasLoader loader
 */
class BillingServiceProvider extends ServiceProvider
{
    /**
     * BillingServiceProvider constructor.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);

        $this->loader = AliasLoader::getInstance();
        $this->app = $app;
    }

    public function register()
    {
        $this->authenticationServiceProvider();
        $this->customerServiceProvider();
        $this->cartServiceProvider();
        $this->productServiceProvider();
    }

    private function authenticationServiceProvider()
    {
        $this->app->bind('Authentication',function() {
            return new \App\Billing\Classes\Authentication;
        });
        $this->loader->alias('Authentication', \App\Billing\Facades\Authentication::class);
    }

    private function customerServiceProvider()
    {
        $this->app->bind('Customer',function() {
            return new \App\Billing\Classes\Customer;
        });
        $this->loader->alias('Customer', \App\Billing\Facades\Customer::class);
    }

    private function cartServiceProvider()
    {
        $this->app->bind('Cart',function() {
            return new \App\Billing\Classes\Cart;
        });
        $this->loader->alias('Cart', \App\Billing\Facades\Cart::class);
    }

    private function productServiceProvider()
    {
        $this->app->bind('Product',function() {
            return new \App\Billing\Classes\Product;
        });
        $this->loader->alias('Product', \App\Billing\Facades\Product::class);
    }

}
