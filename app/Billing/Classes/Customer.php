<?php

namespace App\Billing\Classes;

use App\User;

class Customer extends Billing
{
    /**
     * Returns customer profile with subscription payment methods
     * and addresses
     *
     * @param $user
     * @param int $track_id
     * @param bool $isRaw
     * @return array
     */
    public function find($user, $track_id = null, $isRaw = false)
    {
        $id = $user;

        if (is_object($user)) {
            $id = $user->id;
        }

        $url = 'customer/' . $id;

        if ($track_id) {
            $url .= '/track/' . $track_id;
        }

        $response = $this->getData($url);
        if ($isRaw == true) {
            return $response;
        }

        $code = $response->getStatusCode();
        $body = $response->getBody();
        $json = json_decode($body);

        if ($code == self::HTTP_OK) {

            return $json;

        }

        return null;

    }

    /**
     * Returns products a customer has purchased
     *
     * @param $user
     * @param string $classification IN all, virtual, physical
     * @param bool $isRaw
     * @return array
     */
    public function getPurchases($user, $classification = 'all')
    {

        $id = $user;

        if (is_object($user)) {
            $id = $user->id;
        }

        if (!in_array($classification, ['all', 'virtual', 'physical'])) {
            $classification = 'all';
        }

        $postData = [
            'client_customer_id' => $id,
            'classification'     => $classification,
        ];

        $response = $this->postData('cart-history', $postData);

        $code = $response->getStatusCode();
        $json = json_decode($response->getBody());

        if ($code == self::HTTP_OK) {
            return $json;
        }

        return null;

    }


    /**
     * create new customer
     *
     * @param  \App\User $user
     * @param bool $temporaryId
     * @return bool
     */
    public function create($user, $temporaryId = false)
    {
        if ($user) {

            $postData = [
                'client_customer_id' => $user->id,
                'email'              => $user->email,
            ];

            if (!empty($user->first_name)) {
                $postData['first_name'] = $user->first_name;
            }
            if (!empty($user->last_name)) {
                $postData['last_name'] = $user->last_name;
            }

            $postData['acquisition_channel'] = 'ome';

            if ($temporaryId !== false) {
                $postData['tmp_client_customer_id'] = $temporaryId;
            }

            $response = $this->postData('customer', $postData);

            $code = $response->getStatusCode();

            // HTTP_CONFLICT means the user already is a customer (which is fine)
            if ($code == self::HTTP_OK || $code == self::HTTP_CONFLICT) {

                return $user->id;
            }
        }

        return false;
    }

    /**
     * get customer or create customer if does not exist
     *
     * @param  \App\User $user
     * @param bool $temporaryId
     * @return bool
     */
    public function getOrCreate($user, $track_id = null, $temporaryId = false)
    {
        $customerId = $this->create($user, $temporaryId);
        if ($customerId !== false) {
            return $this->find($customerId, $track_id);
        }
        return $this->standardResponse('An error occurred fetching the customer. Please try again later.', null, false);
    }

}
