<?php

namespace App\Billing\Classes;

use Carbon\Carbon;
use Config;
use DB;

class Authentication extends Billing
{
    /**
     * fetches a new key from the API and saves value to the database.
     *
     * @return bool
     */
    public function refreshKey()
    {
        $json = [];
        try {

            $payload = [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => Config::get('billing.client.id'),
                    'client_secret' => Config::get('billing.client.secret'),
                    'username' => Config::get('billing.username'),
                    'password' => Config::get('billing.password'),
                ],
            ];
            $response = $this->client->post('oauth/token', $payload);
            $json = json_decode($response->getBody());
        } catch (\Exception $e) {
            \Bugsnag::notifyException($e);
            $this->handleGuzzleException($e);
        }
        if (is_object($json) && property_exists($json, 'access_token')) {
            // replace current key with new key
            DB::table('api_keys')->where('property_id', Config::get('billing.property.id'))->delete();
            $now = Carbon::now()->toDateTimeString();
            DB::table('api_keys')->insert([
                'token_type' => $json->token_type,
                'expires_in' => $json->expires_in,
                'access_token' => $json->access_token,
                'refresh_token' => $json->refresh_token,
                'property_id' => config('billing.property.id'),
                'created_at' => $now,
                'updated_at' => $now,
            ]);

            return true;
        }

        return false;
    }
}
