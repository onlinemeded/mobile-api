<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SystemDeck extends Model
{
    protected $table = 'flashcard_system_decks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flashcard_system_deck_type_id',
        'user_id',
        'updated_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function flashcards()
    {
        return $this->belongsToMany(
            'App\Flashcard',
            'flashcard_system_deck_flashcards',
            'flashcard_system_deck_id',
            'flashcard_id'
            )
            ->withTimestamps();
    }


    public function flashcardsByTrack()
    {
        // get all flashcards within the current track
        $flashcardIds = app('Dingo\Api\Auth\Auth')->user()
            ->careerTrack->getFlashcards()
            ->pluck('id');

        // using the track specific flashcards, grab the ones that overlap with the system decks
        return $this->belongsToMany(
            'App\Flashcard',
            'flashcard_system_deck_flashcards',
            'flashcard_system_deck_id',
            'flashcard_id'
        )
            ->whereIn('flashcard_id', $flashcardIds)
            ->withTimestamps();
    }
}
