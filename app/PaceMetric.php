<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class PaceMetric extends Model
{
    public static $readAction            = 'read';
    public static $watchedAction         = 'watched';
    public static $answeredAction        = 'answered';
    public static $listenedAction        = 'listened';
    public static $downloadedNotesAction = 'downloaded_notes';
    public static $downloadedAudioAction = 'downloaded_audio';

    protected $fillable = [
        'user_id',
        'metric_id',
        'metric_type',
        'description',
    ];

    public static function createPaceMetric($userId, $action, $metricType, $metricId)
    {
        $exists = self::where('metric_type', $metricType)
            ->where('user_id', $userId)
            ->where('description', $action)
            ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
            ->exists();

        if (!$exists) {
            return self::create([
                'user_id' => $userId,
                'metric_id' => $metricId,
                'metric_type' => $metricType,
                'description' => $action,
            ]);
        }
        return false;
    }
}
