<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    /**
     * The mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'code', 'completed', 'completed_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Generate a random token for activation code.
     *
     * @return string
     */
    public static function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    /**
     * Create an activation token for a user.
     *
     * @param  object $user
     * @return string
     */
    public function createActivation($user)
    {
        $activation = $this->getActivation($user);

        if (!$activation) {
            return $this->createToken($user);
        }

        return $this->regenerateToken($user);
    }

    /**
     * Get the activation relationship for a user.
     *
     * @param  object $user
     * @return mixed
     */
    public function getActivation($user)
    {
        return Activation::where('user_id', $user->id)->first();
    }

    /**
     * Retrieve an activation by token.
     *
     * @param  string $token
     * @return mixed
     */
    public function getActivationByToken($token)
    {
        return Activation::where('code', $token)->first();
    }

    /**
     * Delete an activation by the token.
     *
     * @param  string $token
     * @return mixed
     */
    public function deleteActivation($token)
    {
        Activation::where('code', $token)->delete();
    }

    /**
     * Generate a new token for a user.
     *
     * @param  object $user
     * @return string
     */
    private function regenerateToken($user)
    {
        $token = static::getToken();
        Activation::where('user_id', $user->id)->update([
            'code' => $token,
            'created_at' => new Carbon()
        ]);

        return $token;
    }

    /**
     * Create a new token for a user.
     *
     * @param  object $user
     * @return string
     */
    private function createToken($user)
    {
        $token = static::getToken();
        Activation::insert([
            'user_id' => $user->id,
            'code' => $token,
            'created_at' => new Carbon()
        ]);

        return $token;
    }
}
