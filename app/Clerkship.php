<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clerkship extends Model
{
    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function flashcards()
    {
        return $this->hasMany('App\Flashcard');
    }
}
