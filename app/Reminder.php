<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReminderEmail;
use App\Mail\SocialPasswordEmail;

class Reminder extends Model
{

    protected $fillable = ['user_id', 'code'];

    /**
     * Generate a random password reset token.
     *
     * @return string
     */
    public function createToken()
    {
        return hash_hmac('sha256', str_random(32), config('app.key'));
    }

    /**
     * Retrieve the reminder by the user id.
     *
     * @param  object  $user
     * @return mixed
     */
    public function getReminder($user)
    {
        return Reminder::where('user_id', $user->id)->first();
    }

    /**
     * Check if the user has a reminder that is not completed.
     * If one doesn't exist, create one.
     *
     * @param  object $user
     * @return object
     */
    public function checkIfReminder($user)
    {
        $reminder = $this->getReminder($user);

        if ($reminder && $reminder->completed || ! $reminder) {
            if ($reminder) {
                $reminder->delete();
            }

            return Reminder::create([
                'user_id' => $user->id,
                'code'    => $this->createToken()
            ]);
        }

        return $reminder;
    }

    /**
     * Retrieve the reminder by the token.
     *
     * @param  string $token
     * @return mixed
     */
    public function getReminderByToken($token)
    {
        return Reminder::where('code', $token)->first();
    }

    /**
     * Send the reminder (password reset) email.
     *
     * @param  string $email
     */
    public function emailReminder($email) {

        $user = User::find($this->user_id);
        $initial_failures = sizeof(Mail::failures());

        if($user) {
            Mail::to($email)->send(new PasswordReminderEmail($this->code, $user));
        }

        if ($initial_failures == sizeof(Mail::failures())) {
            return 'Success! Password reset email sent.';
        } else {
            return 'Error. Email not sent';
        }

    }

    public function emailSocialSetup($email) {
        $user = User::find($this->user_id);
        $initial_failures = sizeof(Mail::failures());

        if($user) {
            Mail::to($email)->send(new SocialPasswordEmail($this->code, $user));
        }

        if ($initial_failures == sizeof(Mail::failures())) {
            return true;
        } else {
            return false;
        }
    }

}
