<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Topic extends Model
{

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function flashcards()
    {
        return $this->hasMany('App\Flashcard');
    }

    public function questions()
    {
        return $this->hasMany('App\QBankQuestion');
    }

    public function recentUsers()
    {
        return $this->hasMany('App\TopicHistory');
    }

    public function getWrappedUrl($contentType)
    {
        switch ($contentType) {
            case 'video':
            case 'video_cc':
            case 'audio':
            case 'notes':
            case 'whiteboard':
                return "/topic/asset-url/$contentType/$this->id";
            default:
                throw new \Exception("Unsupported type cant not get wrapped URL");
        }
    }

    public function getNoteUrlAttribute($excludeAuthority = false) {
        $name = str_replace([':','/'], '', $this->category->name.' - '.$this->name.'.pdf');
        
        if ($excludeAuthority) {
            return $name;
        }

        return config('content.s3_bucket_url') . config('content.notes_path') . $name;
    }

    public function getWhiteboardUrlAttribute($excludeAuthority = false)
    {
        $name = $this->category->slug."/".$this->slug.".pdf";
        if ($excludeAuthority) {
            return config('content.whiteboards_path') . $name;
        }
        return config('content.s3_bucket_url') . config('content.whiteboards_path') . $name;
    }

    public function getRawVideoDurationAtrribute() {
        // then get the unformatted value
        return $this->getOriginal('duration');
    }
}
