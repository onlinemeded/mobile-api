<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackProductPermissions extends Model
{
	protected $table = 'track_product_permissions';
	protected $fillable = ['user_id', 'track_id', 'product_id', 'permission_id'];

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}

	public function track()
	{
		return $this->belongsTo(\App\Track::class);
	}

	public function permission()
	{
		return $this->belongsTo(\App\Permission::class);
	}
}
