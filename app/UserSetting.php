<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{

    protected $table = 'flashback_user_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'include_step_2', 'include_step_3',
        'timer_7_day', 'timer_14_day', 'timer_30_day',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
