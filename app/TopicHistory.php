<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicHistory extends Model
{
    protected $table = 'topic_history';

    protected $dates = [
        'created_at', 'updated_at', 'time', 'last_synced_at', 'reviewed_at', 'next_due_at',
    ];

    protected $fillable = ['user_id', 'topic_id', 'time'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }
}
