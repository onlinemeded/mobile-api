<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerSupportFeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    /**
     * The activation url.
     * @var object $user
     * @var string $feedbackText
     */
    public $user;
    public $feedbackText;
    
     /**
     * Create a new message instance.
     *
     * @param object $user
     * @param string $feedbackText
     */

    public function __construct($user, $feedbackText)
    {
        $this->user = $user;
        $this->feedbackText = $feedbackText;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('New Mobile Application Feedback')
            ->view('email.customer-support-feedback');
    }
}
