<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReminderEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $url;
    
    public function __construct($code, $user)
    {
        $this->url = config('main.url') . "/reset/" . $user->id . "/" . $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset your account password.')
                    ->view('email.password-reminder');
    }
}
