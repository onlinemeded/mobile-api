<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CronJobDeveloperEmail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Duration
   *
   * @var integer
   */
  private $duration;

  /**
   * Details
   *
   * @var string
   */
  private $details;

  /**
   * Type
   *
   * @var string
   */
  private $type;

    /**
     * Create a new message instance.
     *
     * @param $duration
     * @param $details
     * @param string $type
     * @internal param string $message
     * @internal param string $email the "from" email
     */
  public function __construct($duration, $details, $type)
  {
    $this->duration = $duration;
    $this->details  = $details;
    $this->type  = $type;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->subject('Cron Job Complete: ' . $this->type)
                ->from(config('mail.from.address'))
                ->to(config('mail.developer.address'))
                ->view('emails.developer.cron')
                ->with([
                  'duration'  => $this->duration,
                  'details' => $this->details,
                  'name' => $this->type,
                  'environment' => ucfirst(\App::environment())
                ]);
  }
}
