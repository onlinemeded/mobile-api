<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivateEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The activation url.
     *
     * @var string
     */
    public $url;

    /**
     * Create a new message instance.
     *
     * @param string $activationCode
     * @param object $user
     */
    public function __construct($activationCode, $user)
    {
        $this->url = config('main.url') . "/activate/$user->id/$activationCode";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Activate Your Account')
                    ->view('email.activate');
    }
}
