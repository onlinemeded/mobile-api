# jump into the relevant directory
cd /home/forge/{{$var['site.web.name']}}

# clone new release
git pull

# remove composer.lock
rm composer.lock

# install composer dependencies
composer install --no-interaction --no-dev --prefer-dist

# clear caches
php artisan route:clear
php artisan config:clear
php artisan view:clear
php artisan cache:clear

# create cache
php artisan config:cache

# npm install
npm install

# npm compile
npm run prod